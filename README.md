#AliCloud OpenSearch DotNet SDK (v3.0)

AliCloud OpenSearch DotNet SDK allows you to build index application on AliCloud. 

##Features
- Index application - Create/View/List/Delete indexes
- Doc - Upload/Delete/Edit/View doc
- Search - config/sort/aggregate/distinct/kvpairs/fetch_fields/disab;e/first_formula_name/formula_name/summary
- Error logs

## Requirements

- AliCloud account: you need to first create an AliCloud if you don't have.
- Active OpenSearch feature: you need to first active OpenSearch feature in AliCloud management console

## Code samples

First, get api access key id and security from AliCloud management console. And include AliCloud.OpenSearch.dll to your project.

More runable code samples can be found in unit test codes.

### Application Managemnet
To perform OpenSearch operation you will first initialize an CloudSearchAccount with api access key id and secret.

```csharp
string AccessKeyId = "YourAccessKeyId";                    // Replace with your access key id
string AccessKeySecret = "YourAccessKeySecret";            // Replace with your access key id
var credential = new CloudSearchCredential(accessKeyId, accessKeySecret);
var account = new CloudSearchAccount(credential);
```

Add split file and threshold manager of upload api, upload file not work yet.

Now, to list all existing application
```csharp
await account.ListApplicationsAsync(CancellationToken.None)
```

to create an index application using the client
```csharp
string appName = "YourAppName";                 // Application name you want to create
string templateName = "TemplateName";           // Can be either build in templates (e.g, builtin_news) or user created templates.
var app = this.account.GetApplicationReferance(name);
app.TemplateName = templateName;
var result = await app.CreateAsync();
```

to view status of an application
```csharp
var result = await app.FetchStatusAsync(CancellationToken.None);
```

to delete an application
```csharp
await app.DeleteIfExistsAsync(CancellationToken.None)
```

to get suggestion list
```csharp
string query = "";               // Your query keyword
string suggestName = "";         // Your suggestion name
int suggestionCount = 3;
var result = await app.GetSuggestListAsync(query, suggestionCount, suggestName, CancellationToken.None);
```

to get error logs
```csharp
int page = 1;
int pageSize = 20;
var sortMode = CloudSearchSortMode.ASC;
var result = await app.GetErrorLogs(page, pageSize, sortMode, CancellationToken.None);
```

### Documnent Managemnet
to create  
```csharp
await app.AddTableRaws<T>("YourTableName", rawData, CancellationToken.None);
```

to update  
```csharp
await app.UpdateTableRaws<T>("YourTableName", rawData, CancellationToken.None);
```

to delete  
```csharp
await app.DeleteTableRaws<T>("YourTableName", rawData, CancellationToken.None);
```

to delete by key 
```csharp
await app.DeleteTableRawsByPrimaryKeys("YourTableName", "id", CancellationToken.None);
```

to upload from a file
```csharp
string tableName = "";            // Input table name you want to import
string filePath = "";             // Full path of the file to upload 
await app.UploadTableRowsFromJsonFile(DocUploadTableName, filePath, CancellationToken.None);
```
to upload from a json string
```csharp
string json = "";
await app.UploadTableRowsByJson(TestConstants.DocUploadTableName, json, CancellationToken.None);
```

### Search
to perform a search, just simple call app.SearchAsync, it receives 3 parameters (SearchOption / CloudSearchExpressions / SearchConfiguration) to allow advanced customization.

Please note that you don't need to specify all the parameters, just pass what you need.

```csharp
// search configurations for start/hit/format/rerank_size, this is optional
var configuration = new SearchConfiguration()
{
    Start = 1,
    Hit = 20,
    Format = SearchFormat.Json,
    RerankSize = 200
};

// search expressions for query/sort/filter/aggregate/distinct
// if query expression is not specified, default:'' will be used.
var queryExpression = new QueryExpression("default", "keyword");
var sortExpression = CloudSearchSort.GenerateSortExpression(SortType.Desc, "create_timestamp");
var filter = CloudSearchFilter.GenerateFilterCondition("cat_id", FilterComparison.EqualsTo, 28);
var filterExpression = new FilterSingleExpression(filter);
var aggregateExpression = new AggregateExpression { GroupKey = "cat_id", AggFun = "count()" };
var distinctExpression = new DistinctExpression { DistKey = "title" };
var kvpairsExpression = new KeyValuePairExpression("key1", "value1"));

var searchExpression = new CloudSearchExpressions();
searchExpression.QueryExpression = queryExpression;
searchExpression.SortExpression = sortExpression;
searchExpression.FilterExpression = filterExpression;
searchExpression.AggregateExpression = aggregateExpression;
searchExpression.DistinctExpression = distinctExpression;
searchExpression.KeyValuePairExpression = kvpairsExpression;

// search options for fetch_fileds/disable/first_formula_name/formula_name/summary, this is optional
var summary = new Summary() { Field = "id" };
var searchOption = new SearchOption();
searchOption.FetchFields = new List<string> {"id", "title"};
searchOption.Disable = "";
searchOption.FirstFormulaName = "";
searchOption.FormulaName = "";
searchOption.Summary = summary;

var result = await app.SearchAsync(new SearchOption(), queryExpression, configuration, CancellationToken.None);
```

combined expression are supported too, see following examples
```csharp
var queryExpression1 = CloudSearchQuery.GenerateQueryExpression("title", "keyword1");
var queryExpression2 = CloudSearchQuery.GenerateQueryExpression("title", "keyword2");
var combineQueryExpression = CloudSearchQuery.GenerateCombineQueryExpression(queryExpression1, QueryOperator.And, queryExpression2); 

var filter1 = CloudSearchFilter.GenerateFilterCondition("cat_id", FilterComparison.EqualsTo, 28);
var filter2 = CloudSearchFilter.GenerateFilterCondition("hit_num", FilterComparison.GreaterThan, 1);
var filterExpression = new FilterExpression(filter1, FilterLogicOperator.And, filter2);

var combineSortExpression = CloudSearchSort.GenerateCombineSortExpression(new SortExpression[]
{
    CloudSearchSort.GenerateSortExpression(SortType.Desc, "create_timestamp"),
    CloudSearchSort.GenerateSortExpression(SortType.Asc, "RANK")
});
```

to run a search across applications, call account.SearchAsync() and pass in app name list
```csharp
var appNames = new string[] { "appName1", "appName2", "appName3", };
var queryExpression = CloudSearchQuery.GenerateQueryExpression("default", "keyword");
var result = await account.SearchAsync(appNames, searchOption, queryExpression, null, CancellationToken.None);
```

### Error handle
If there are errors when perform an operation, an CloudSearchException will be thrown, you can check error detail by accessing ErrorCode and Message property.
```csharp
catch (CloudSearchException ex)
{
    //ex.ErrorCode
    //ex.Message
}
```

More code samples can be found at unit test codes under AliCloud.OpenSearch.Test project

## OpenSearch restful api and doc bug list
Following lists OpenSearch restful api/doc bugs encountered during OpenSearch DotNet SDK development.
- Create index app only supports POST, but document says both POST and GET are supported
- If an app exists but not actived, error message should be "app not actived" instead of "app not exist"
- Suggestion api does not return Status like other api, it's expected all api return schema should be consistent.
- Clikc first_formula_name and formula_name in help page will be redirect to 404 page. 
- Create app normally returns within just seconds but sometimes it can take a few minutes to complete. The SLA is not predictable.

## Unit Test Status
To run unit test, first update TestConstants.cs with your AliCloud configuration. Follow instructions in TestConstants.cs to create a pre-configured application prior to run unit test.

Test might intermitment fail with timeout due to server side issues, just simple rerun the test will pass.

Latest test pass status:

![test](screenshot/coverage.png)

![test](screenshot/unittest.png)

