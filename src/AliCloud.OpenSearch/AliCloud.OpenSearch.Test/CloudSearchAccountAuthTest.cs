﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using AliCloud.OpenSearch.Auth;
using AliCloud.OpenSearch.Common;
using AliCloud.OpenSearch.Exceptions;

namespace AliCloud.OpenSearch.Test
{
    [TestClass]
    public class CloudSearchAccountAuthTest : CloudSearchTestBase
    {
        public CloudSearchAccountAuthTest()
        {
        }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
        }

        [TestMethod]
        [Description("Test auth with incorrect api access id and key")]
        public async Task CloudSearchAccountTest_Auth()
        {
            try
            {
                var credential = new CloudSearchCredential("fakekeyid", "fakekeysecret");
                var account = new CloudSearchAccount(credential);
                var cloudSearchDocClient = account.CreateCloudSearchIndexClient();
                await cloudSearchDocClient.GetIndexListAsync(CancellationToken.None);
            }
            catch (CloudSearchException ex)
            {
                Assert.AreEqual(5001, ex.ErrorCode);
            }
        }
    }
}
