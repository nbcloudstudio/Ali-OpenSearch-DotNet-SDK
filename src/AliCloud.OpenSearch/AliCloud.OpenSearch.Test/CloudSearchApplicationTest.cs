﻿using AliCloud.OpenSearch.Common;
using AliCloud.OpenSearch.Exceptions;
using AliCloud.OpenSearch.Model;
using AliCloud.OpenSearch.Model.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AliCloud.OpenSearch.Test
{
    [TestClass]
    public class CloudSearchApplicationTest : CloudSearchTestBase
    {

        [TestMethod]
        public async Task CloudSearchApplicationTest_TestListApplication_Segment()
        {
            int page = 1;
            int pageSize = 1;
            var segment = await this.account.GetApplicationListSegmentAsync(page, pageSize, CancellationToken.None);

            Assert.AreEqual(segment.Page, page);
            Assert.AreEqual(segment.PageSize, pageSize);
            if (segment.Total == 0)
            {
                Assert.IsTrue(segment.Applications == null || segment.Applications.Count() == 0);
            }
            else if (segment.Total > segment.PageSize)
            {
                Assert.AreEqual(segment.Applications.Count(), segment.PageSize);
            }
            else
            {
                Assert.AreEqual(segment.Applications.Count(), segment.Total);
            }
        }

        [TestMethod]
        public void CloudSearchApplicationTest_TestListApplication_All()
        {
            int page = 1;
            int pageSize = 1;
            var segment = this.account.GetApplicationListSegment(page, pageSize);
            var all = this.account.ListApplications();
            if (segment.Total > 0)
            {
                Assert.AreEqual(segment.Total, all.Count());
            }
            else
            {
                Assert.AreEqual(all.Count(), 0);
            }
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_TestCreate_Delete_Application()
        {
            var app = this.GenerateTestApplication();
            app.Create();

            var all = await this.account.ListApplicationsAsync(CancellationToken.None);
            Assert.IsTrue(all.Any(a => a.Name == app.Name));

            app.Delete();

            all = await this.account.ListApplicationsAsync(CancellationToken.None);
            Assert.IsTrue(!all.Any(a => a.Name == app.Name));
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_TestDoubleCreate_Application()
        {
            var app = this.GenerateTestApplication();
            await app.CreateAsync(CancellationToken.None);
            try
            {
                await app.CreateAsync(CancellationToken.None);
            }
            catch (CloudSearchException ex)
            {
                Assert.AreEqual(ex.ErrorCode, CloudSearchErrorCodes.AppAlreadyExists);
            }

            // this should not throw out any exception 
            await app.CreateIfNotExistsAsync(CancellationToken.None);

            // this should not throw out any exception too.
            app.CreateIfNotExists();
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_TestDoubleDelete_Application()
        {
            var app = this.GenerateTestApplication();
            await app.CreateAsync(CancellationToken.None);
            await app.DeleteAsync(CancellationToken.None);

            try
            {
                await app.DeleteAsync(CancellationToken.None);
            }
            catch (CloudSearchException ex)
            {
                Assert.AreEqual(ex.ErrorCode, CloudSearchErrorCodes.AppNotExists);
            }

            // this should not throw out any exception
            app.DeleteIfExists();
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_Create_IllegalName()
        {
            try
            {
                var app = this.GenerateTestApplication();
                app.Name = "1-2-3-4";

                await app.CreateAsync(CancellationToken.None);
                Assert.Fail();
            }
            catch (CloudSearchException ex)
            {
                Assert.AreEqual(CloudSearchErrorCodes.IllegalAppName, ex.ErrorCode);
            }
        }

        [TestMethod]
        public void CloudSearchApplicationTest_GetSatus()
        {
            var app = this.GetPreConfiguredTestApplication();
            app.FetchStatus();

            Assert.IsNotNull(app.Status);
            Assert.AreEqual(app.Name, app.Status.IndexName);
            Assert.IsTrue(app.Status.Indexes.Count > 0);
            Assert.IsTrue(app.Status.Fields.FromTable.Tables.Count > 0);
            Assert.IsTrue(app.Status.Quota.Count > 0);
            Assert.IsTrue(app.Status.Fields.FromTable.Tables.GetItems().ElementAt(0).Value.Columns.Count > 0);
            Assert.IsTrue(app.Status.Fields.FromTable.JoinMap != null);
            Assert.IsTrue(app.Status.Fields.FromTable.JoinMap.GetItems() != null);

            foreach (var table in app.Status.Fields.FromTable.Tables)
            {
                Assert.IsNotNull(table);
            }

            Assert.IsTrue(app.Status.Fields.FromTable.Tables.Count == ((ICloudSearchItemCollection)(app.Status.Fields.FromTable.Tables)).GetItems().Count);
        }

        [TestMethod]
        public void CloudSearchApplicationTest_GetSuggestionList()
        {
            try
            {
                var app = GetPreConfiguredTestApplication();
                string query = "搜索";
                string suggestName = TestConstants.SuggestRuleName;
                int suggestionCount = 3;
                // Known to fail, due to suggestion api does not follow standard json return format.
                var result = app.GetSuggestList(query, suggestionCount, suggestName);
                Assert.IsNotNull(result.Count() > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
            }
        }

        [TestMethod]
        public void CloudSearchApplicationTest_RebuildIndex()
        {
            try
            {
                var app = this.GenerateTestApplication();
                app.CreateIfNotExists();
                var task = app.RebuildIndex();
                Assert.IsTrue(task.TaskId != null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
                throw;
            }
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_RebuildIndex_WithImport()
        {
            try
            {
                var app = this.GenerateTestApplication();
                app.CreateIfNotExists();
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), "TestData", TestConstants.DocUploadFileName);
                string json = File.ReadAllText(filePath);
                app.UploadTableRowsByJson(TestConstants.DocUploadTableName, json);
                var task = await app.RebuildIndexAsync("main", CancellationToken.None);
                Assert.IsTrue(task.TaskId != null);
            }
            catch (Exception ex)
            {
                if (ex is CloudSearchException && (ex as CloudSearchException).ErrorCode == 10303)
                {
                    return;
                }
                else
                {
                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine(ex.StackTrace);
                    throw;
                }
            }
        }

        [TestMethod]
        public void CloudSearchApplicationTest_RebuildIndex_WithImport_Sync()
        {
            try
            {
                var app = this.GenerateTestApplication();
                app.CreateIfNotExists();
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), "TestData", TestConstants.DocUploadFileName);
                string json = File.ReadAllText(filePath);
                app.UploadTableRowsByJson(TestConstants.DocUploadTableName, json);
                var task = app.RebuildIndex("main");
                Assert.IsTrue(task.TaskId != null);
            }
            catch (Exception ex)
            {
                Exception innerException = null;
                if (ex is AggregateException)
                {
                    innerException = ((AggregateException)ex).InnerExceptions.First();
                }
                else
                {
                    innerException = ex;
                }

                if (innerException is CloudSearchException && (innerException as CloudSearchException).ErrorCode == 10303)
                {
                    return;
                }
                else
                {
                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine(ex.StackTrace);
                    throw;
                }
            }
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_GetErrorLogs()
        {
            var app = GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);

            int page = 1;
            int pageSize = 20;
            var sortMode = CloudSearchSortMode.ASC;

            var result = app.GetErrorLogs(page, pageSize, sortMode);
        }
    }
}
