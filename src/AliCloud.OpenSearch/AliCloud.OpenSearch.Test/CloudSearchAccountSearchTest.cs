﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchAccountSearchTest.cs" company="">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

/// <summary>
/// The Test namespace.
/// </summary>
namespace AliCloud.OpenSearch.Test
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Threading;
    using AliCloud.OpenSearch.Model.Search;

    /// <summary>
    /// Class CloudSearchAccountSearchTest.
    /// </summary>
    [TestClass]
    public class CloudSearchAccountSearchTest : CloudSearchTestBase
    {
        /// <summary>
        /// The default keyword
        /// </summary>
        private const string DefaultKeyword = "搜索";

        /// <summary>
        /// Clouds the search account test_ search_ with keyword.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with single query")]
        public void CloudSearchAccountTest_Search_WithSingleQuery()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

            var queryExpression = new QueryExpression("title", DefaultKeyword, 70);
            var result = app.Search(new CloudSearchOption(), queryExpression, null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
            // todo: assert item result contains keyword
        }

        /// <summary>
        /// Clouds the search account test_ search_ with combine query.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with combiled query")]
        public async Task CloudSearchAccountTest_Search_WithCombineQuery()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

            var queryExpression1 = new QueryExpression("title", DefaultKeyword);
            var queryExpression2 = string.Format("{0}:{1}", "title", DefaultKeyword);
            var combinedQueryExpression = new QueryCombineExpression(queryExpression1, QueryOperator.And, queryExpression2);
            var result = await app.SearchAsync(new CloudSearchOption(), combinedQueryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with configuration.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with config")]
        public async Task CloudSearchAccountTest_Search_WithConfig()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var configuration = new CloudSearchConfiguration()
            {
                Start = 1,
                Hit = 20,
                Format = SearchFormat.Json,
                RerankSize = 200
            };
            var result = await app.SearchAsync(new CloudSearchOption(), queryExpression, configuration, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with single sort.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with sort")]
        public void CloudSearchAccountTest_Search_WithSingleSort()
        {
            var app = GetPreConfiguredTestApplication();

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var sortExpression = new SortExpression(SortType.Desc, "create_timestamp");
            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.SortExpression = sortExpression;

            var result = app.Search(new CloudSearchOption(), searchExpression, null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
            // todo: assert item 2 create_timestamp > item1 create_timestamp
        }

        /// <summary>
        /// Clouds the search account test_ search_ with single sort.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with combine sort")]
        public async Task CloudSearchAccountTest_Search_WithCombineSort()
        {
            var app = GetPreConfiguredTestApplication();

            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var combineSortExpression = CloudSearchSort.GenerateCombineSortExpression(new SortExpression[]
            {
                CloudSearchSort.GenerateSortExpression(SortType.Desc, "create_timestamp"),
                CloudSearchSort.GenerateSortExpression(SortType.Asc, "RANK")
            });
            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.SortExpression = combineSortExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
            // todo: assert item 2 create_timestamp > item1 create_timestamp
        }

        [TestMethod]
        [Description("Test search query with single filter")]
        public async Task CloudSearchAccountTest_Search_WithSinleFilter()
        {
            var app = GetPreConfiguredTestApplication();

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var filterExpression = new FilterExpression("cat_id", FilterComparison.EqualsTo, 28);

            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.FilterExpression = filterExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        [TestMethod]
        [Description("Test search query with single filter")]
        public async Task CloudSearchAccountTest_Search_WithComplexFilter()
        {
            var app = GetPreConfiguredTestApplication();

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var filterExpression1 = new FilterExpression("cat_id", FilterComparison.EqualsTo, 28);
            var filterExpression2 = new FilterExpression("hit_num", FilterComparison.GreaterThan, 1);
            var filterExpression = new FilterCombineExpression(filterExpression1, FilterLogicOperator.Or, filterExpression2);

            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.FilterExpression = filterExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        [TestMethod]
        [Description("Test search query with combine filters")]
        public async Task CloudSearchAccountTest_Search_WithCombineFilters()
        {
            var app = GetPreConfiguredTestApplication();

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var filter1 = new FilterExpression("cat_id", FilterComparison.EqualsTo, 28);
            var filter2 = new FilterExpression("hit_num", FilterComparison.GreaterThan, 1);
            var filterExpression = new FilterCombineExpression(filter1, FilterLogicOperator.And, filter2);

            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.FilterExpression = filterExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with aggregate.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with aggregate")]
        public async Task CloudSearchAccountTest_Search_WithAggregate()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var aggregateExpression = new AggregateExpression { GroupKey = "cat_id", AggFun = "count()" };
            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.AggregateExpression = aggregateExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with distinct.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with distinct")]
        public async Task CloudSearchAccountTest_Search_WithDistinct()
        {
            try
            {
                var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

                var queryExpression = new QueryExpression("default", DefaultKeyword);
                var distinctExpression = new DistinctExpression { DistKey = "cat_id" };
                var searchExpression = new CloudSearchExpressions();
                searchExpression.QueryExpression = queryExpression;
                searchExpression.DistinctExpression = distinctExpression;

                var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

                Assert.IsNotNull(result);
                Assert.IsTrue(result.Items.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        /// <summary>
        /// Clouds the search account test_ search_ with kv paris.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search query with kv pairs")]
        public async Task CloudSearchAccountTest_Search_WithKVParis()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);

            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var distinctExpression = new DistinctExpression() { DistKey = "cat_id" };
            var kvpairsExpression = new KeyValuePairExpression("duniqfield", "cat_id");
            var searchExpression = new CloudSearchExpressions();
            searchExpression.QueryExpression = queryExpression;
            searchExpression.KeyValuePairExpression = kvpairsExpression;
            searchExpression.DistinctExpression = distinctExpression;

            var result = await app.SearchAsync(new CloudSearchOption(), searchExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with fetch fields.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with query fetch fields")]
        public async Task CloudSearchAccountTest_Search_WithFetchFields()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var searchOption = new CloudSearchOption();
            searchOption.FetchFields = new List<string> { "id", "title" };
            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var result = await app.SearchAsync(searchOption, queryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with disable.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with disable")]
        public async Task CloudSearchAccountTest_Search_WithDisable()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var searchOption = new CloudSearchOption();
            searchOption.Disable = "qp";
            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var result = await app.SearchAsync(searchOption, queryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the name of the search account test_ search_ with first formula.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with first_formula_name")]
        public async Task CloudSearchAccountTest_Search_WithFirstFormulaName()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var searchOption = new CloudSearchOption();
            searchOption.FirstFormulaName = "";
            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var result = await app.SearchAsync(searchOption, queryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the name of the search account test_ search_ with formula.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with formula_name")]
        public async Task CloudSearchAccountTest_Search_WithFormulaName()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var searchOption = new CloudSearchOption();
            searchOption.FormulaName = string.Empty;
            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var result = await app.SearchAsync(searchOption, queryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with summary.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with summary")]
        public async Task CloudSearchAccountTest_Search_WithSummary()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var summary = new CloudSearchSummary()
            {
                Field = "id"
            };
            var searchOption = new CloudSearchOption();
            searchOption.Summary = summary;
            var queryExpression = new QueryExpression("default", DefaultKeyword);

            var result = await app.SearchAsync(searchOption, queryExpression, null, CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        /// <summary>
        /// Clouds the search account test_ search_ with mutiple applications.
        /// </summary>
        /// <returns>Task.</returns>
        [TestMethod]
        [Description("Test search with 2 applications")]
        public void CloudSearchAccountTest_Search_WithMutipleApplications()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var appNames = new string[] { app.Name, TestConstants.PreConfiguredTestApp };
            var queryExpression = new QueryExpression("default", DefaultKeyword);
            var searchOption = new CloudSearchOption();
            searchOption.FormulaName = string.Empty;

            var result = account.Search(appNames, searchOption, queryExpression, null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        [TestMethod]
        [Description("Test search with 2 applications")]
        public void CloudSearchAccountTest_Search_WithMutipleApplications2()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var appNames = new string[] { app.Name, TestConstants.PreConfiguredTestApp };
            var expressions = new CloudSearchExpressions()
            {
                QueryExpression = new QueryExpression("default", DefaultKeyword)
            };

            var searchOption = new CloudSearchOption();
            searchOption.FormulaName = string.Empty;

            var result = account.Search(appNames, searchOption, expressions, null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }

        [TestMethod]
        public void CloudSearchAccountTest_Search_ByString()
        {
            var app = this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
            var appNames = new string[] { app.Name, TestConstants.PreConfiguredTestApp };

            string queryExpression = string.Format("query={0}:{1}", "default", DefaultKeyword);
            var searchOption = new CloudSearchOption();
            searchOption.FormulaName = string.Empty;

            var result = account.SearchByString(appNames, searchOption, queryExpression);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count > 0);
        }
    }
}
