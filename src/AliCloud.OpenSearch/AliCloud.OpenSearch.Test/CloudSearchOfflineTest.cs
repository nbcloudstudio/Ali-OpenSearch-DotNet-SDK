﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliCloud.OpenSearch.Common;
using AliCloud.OpenSearch.Exceptions;
using AliCloud.OpenSearch.JsonConverters;
using AliCloud.OpenSearch.Model;
using AliCloud.OpenSearch.Model.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AliCloud.OpenSearch.Test
{
    [TestClass]
    public class CloudSearchOfflineTest : CloudSearchTestBase
    {
        [TestMethod]
        public void CloudSearchExceptionManagerTest_FromErrors()
        {
            var errors = new CloudSearchError[]{
                new CloudSearchError(){Code=1,Message = "error 1"},
                new CloudSearchError(){Code =2, Message = "error 2"}
            };

            var exceptions = CloudSearchExceptionManager.FromErrors(errors);
            Assert.IsTrue(exceptions is AggregateException);
            Assert.IsTrue((exceptions as AggregateException).InnerExceptions.Count == 2);
            Assert.IsTrue((exceptions as AggregateException).InnerExceptions.Select(e => (e as CloudSearchException).ErrorCode)
                .Except(errors.Select(e => e.Code)).Count() == 0);
        }

        [TestMethod]
        public void CloudSearchExceptionManagerTest_FromError()
        {
            var error = new CloudSearchError() { Code = 1, Message = "error 1" };

            var exception = CloudSearchExceptionManager.FromError(error);

            Assert.AreEqual(exception.ErrorCode, error.Code);
            Assert.AreEqual(exception.Message, error.Message);
        }

        [TestMethod]
        public void DistinctExpression_Test_Invalid()
        {
            try
            {
                DistinctExpression express = new DistinctExpression();
                var str = express.ToString();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                if (!(ex is ArgumentNullException))
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void CloudSearchConfiguration_Test_Inalid()
        {
            try
            {
                CloudSearchConfiguration config = new CloudSearchConfiguration();
                config.Start = 1000000000;
                var str = config.ToString();
                Assert.Fail();
            }
            catch (Exception ex)
            {
                if (!(ex is ArgumentOutOfRangeException))
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void CloudSearchOfflineTest_RetryPolicy_Clone()
        {
            CloudSearchRetryPolicy policy = new CloudSearchRetryPolicy();
            policy.RetryCondition = response => true;
            policy.RetryInterval = TimeSpan.FromSeconds(1);
            policy.RetryOnException = ex => true;
            policy.RetryTimes = 5;

            CloudSearchRetryPolicy clone = policy.Clone();
            clone.GetType().GetProperties().ToList()
                .ForEach(p =>
                {
                    Assert.AreEqual(p.GetValue(policy), p.GetValue(clone));
                });
        }

        [TestMethod]
        public async Task CloudSearchOfflineTest_RetryPolicy()
        {
            CloudSearchRetryPolicy policy = new CloudSearchRetryPolicy();
            policy.RetryCondition = response => response.Status == CloudSearchStatus.Unkown;
            policy.RetryInterval = TimeSpan.FromSeconds(1);
            policy.RetryOnException = ex => ex is CloudSearchException;
            policy.RetryTimes = 5;

            CloudSearchResponse trueResponse = new CloudSearchResponse() { Status = CloudSearchStatus.Unkown };
            CloudSearchResponse falseResponse = new CloudSearchResponse() { Status = CloudSearchStatus.FAIL };
            CloudSearchException trueException = new CloudSearchException(100, "test");
            Exception falseException = new Exception();

            Assert.IsTrue(await policy.RetryAvaliable(trueResponse, null));
            Assert.IsFalse(await policy.RetryAvaliable(falseResponse, null));
            Assert.IsTrue(await policy.RetryAvaliable(null, trueException));
            Assert.IsFalse(await policy.RetryAvaliable(null, falseException));
        }

        [TestMethod]
        public void CloudSearchOfflineTest_CloudSearchItemCollectionJsonConverter()
        {
            CloudSearchItemCollectionJsonConverter converter = new CloudSearchItemCollectionJsonConverter();
            Assert.IsTrue(converter.CanConvert(typeof(CloudSearchItemCollection<string>)));
        }

        [TestMethod]
        public void CloudSearchOfflineTest_KeyValuePairCombineExpression()
        {
            KeyValuePairExpression exp1 = new KeyValuePairExpression("key1", "value1");
            KeyValuePairExpression exp2 = new KeyValuePairExpression("key2", "value2");
            Assert.AreEqual(exp1.ToString(), "key1:value1");
            KeyValuePairCombineExpression combine = new KeyValuePairCombineExpression(exp1, exp2);
            Assert.AreEqual(combine.ToString(), exp1.ToString() + "," + exp2.ToString());
        }
    }
}
