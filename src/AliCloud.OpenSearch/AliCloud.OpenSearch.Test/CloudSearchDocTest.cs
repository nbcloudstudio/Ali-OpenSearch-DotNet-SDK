﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AliCloud.OpenSearch.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace AliCloud.OpenSearch.Test
{
    [TestClass]
    public class CloudSearchDocTest : CloudSearchTestBase
    {
        [TestMethod]
        public async Task CloudSearchDocTest_AddRaws_ByList()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            var testRaws = UploadTestDataClass.RandomGenerateList();
            await app.AddTableRawsAsync<UploadTestDataClass>(TestConstants.DocUploadTableName, testRaws, CancellationToken.None);
        }

        [TestMethod]
        public async Task CloudSearchDocTest_UpdateRaws_ByList()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            var testRaws = UploadTestDataClass.RandomGenerateList();
            await app.AddTableRawsAsync<UploadTestDataClass>(TestConstants.DocUploadTableName, testRaws, CancellationToken.None);
            app.UpdateTableRaws<UploadTestDataClass>(TestConstants.DocUploadFileName, testRaws);
        }

        [TestMethod]
        public void CloudSearchDocTest_DeleteRaws_ByList()
        {
            var app = this.GenerateTestApplication();
            app.CreateIfNotExists();
            var testRaws = UploadTestDataClass.RandomGenerateList();
            app.AddTableRaws<UploadTestDataClass>(TestConstants.DocUploadTableName, testRaws);
            app.DeleteTableRaws<UploadTestDataClass>(TestConstants.DocUploadFileName, testRaws);
        }

        [TestMethod]
        public void CloudSearchDocTest_AddRaws_ByDataTable()
        {
            var app = this.GenerateTestApplication();
            app.CreateIfNotExists();
            var testRaws = UploadTestDataClass.RandomGenerateDataTable();
            app.AddTableRaws(TestConstants.DocUploadTableName, testRaws);
        }

        [TestMethod]
        public async Task CloudSearchDocTest_UpdateRaws_ByDataTable()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            var testRaws = UploadTestDataClass.RandomGenerateDataTable();
            await app.AddTableRawsAsync(TestConstants.DocUploadTableName, testRaws, CancellationToken.None);
            app.UpdateTableRaws(TestConstants.DocUploadFileName, testRaws);
        }

        [TestMethod]
        public async Task CloudSearchDocTest_Upload_ByCloudSearchTableRaws()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            var testTable = UploadTestDataClass.RandomGenerateDataTable();
            var jarray = JArray.FromObject(testTable);
            var rawsToPost = jarray.Select(raw => new CloudSearchTableRaw() { Command = CloudSearchTableRawCommand.Add, Fields = raw, Timestamp = DateTime.UtcNow.Ticks });
            app.UploadTableRaws(TestConstants.DocUploadFileName, rawsToPost);
        }

        [TestMethod]
        public void CloudSearchDocTest_DeleteRaws_ByDataTable()
        {
            var app = this.GenerateTestApplication();
            app.CreateIfNotExists();
            var testRaws = UploadTestDataClass.RandomGenerateDataTable();
            app.AddTableRaws(TestConstants.DocUploadTableName, testRaws);
            app.DeleteTableRaws(TestConstants.DocUploadFileName, testRaws);
        }

        [TestMethod]
        public async Task CloudSearchDocTest_DeleteRaws_ByKeys()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            var testRaws = UploadTestDataClass.RandomGenerateList();
            await app.AddTableRawsAsync(TestConstants.DocUploadTableName, testRaws, CancellationToken.None);
            var keys = testRaws.Select(r => r.id).ToList();
            app.DeleteTableRawsByPrimaryKeys(TestConstants.DocUploadFileName, keys, "id");
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_UploadFromJsonFile()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "TestData", TestConstants.DocUploadFileName);
            app.UploadTableRowsFromJsonFile(TestConstants.DocUploadTableName, filePath);
        }

        [TestMethod]
        public async Task CloudSearchApplicationTest_UploadByJsonString()
        {
            var app = this.GenerateTestApplication();
            await app.CreateIfNotExistsAsync(CancellationToken.None);
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "TestData", TestConstants.DocUploadFileName);
            string json = File.ReadAllText(filePath);
            app.UploadTableRowsByJson(TestConstants.DocUploadTableName, json);
        }
    }

    public class UploadTestDataClass
    {
        public string id { get; set; }
        public int type_id { get; set; }
        public int cat_id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string url { get; set; }
        public string author { get; set; }
        public string thumbnail { get; set; }
        public string source { get; set; }
        public int create_timestamp { get; set; }
        public int update_timestamp { get; set; }
        public int hit_num { get; set; }
        public int focus_count { get; set; }
        public int grade { get; set; }
        public int comment_count { get; set; }
        public string tag { get; set; }

        public static UploadTestDataClass RandomGenerateSingle()
        {
            Random random = new Random();
            UploadTestDataClass data = new UploadTestDataClass();
            int idx = random.Next(1, 100);
            data.GetType().GetProperties()
                .ToList()
                .ForEach(p =>
                {
                    if (p.PropertyType == typeof(string))
                    {
                        p.SetValue(data, p.Name + "_" + idx.ToString());
                    }
                    else if (p.PropertyType == typeof(int))
                    {
                        p.SetValue(data, idx);
                    }
                });

            return data;
        }

        public static IEnumerable<UploadTestDataClass> RandomGenerateList(int count = 100)
        {
            for (int idx = 0; idx < count; idx++)
            {
                yield return RandomGenerateSingle();
            }
        }

        public static DataTable RandomGenerateDataTable(int count = 100)
        {
            var list = RandomGenerateList(count);
            DataTable table = new DataTable();
            var properties = typeof(UploadTestDataClass).GetProperties().ToList();

            properties.ForEach(p =>
              {
                  table.Columns.Add(p.Name, p.PropertyType);
              });

            list.ToList()
                .ForEach(raw =>
                {
                    DataRow row = table.NewRow();
                    properties.ForEach(p =>
                    {
                        row[p.Name] = p.GetValue(raw);
                    });

                    table.Rows.Add(row);
                });

            return table;
        }
    }
}
