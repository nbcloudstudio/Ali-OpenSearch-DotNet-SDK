﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliCloud.OpenSearch.Auth;
using System.Threading;
using AliCloud.OpenSearch.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AliCloud.OpenSearch.Test
{
    [TestClass]
    public class CloudSearchTestBase
    {
        protected string TestAppPrefix = "actest_";

        protected CloudSearchAccount account;
        private const string BuiltinNewsTemplate = "builtin_news";

        private static Lazy<List<char>> CharsPool = new Lazy<List<char>>(() =>
        {
            List<char> chars = new List<char>();
            chars.AddRange(ParallelEnumerable.Range(48, 10).Select(i => (char)i));// 0-9
            chars.AddRange(ParallelEnumerable.Range(65, 26).Select(i => (char)i));// A-Z
            chars.AddRange(ParallelEnumerable.Range(97, 26).Select(i => (char)i));// a-z
            return chars;
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        public CloudSearchTestBase()
        {
            this.account = new CloudSearchAccount(new CloudSearchCredential(TestConstants.AccessKeyId, TestConstants.AccessKeySecret));
        }

        protected CloudSearchApplication GenerateTestApplication()
        {
            string name = this.TestAppPrefix + this.GenerateRandomAppName();
            var app = this.account.GetApplicationReferance(name);
            app.TemplateName = BuiltinNewsTemplate;

            return app;
        }

        protected CloudSearchApplication GetPreConfiguredTestApplication()
        {
            return this.account.GetApplicationReferance(TestConstants.PreConfiguredTestApp);
        }

        protected string GenerateRandomAppName(int length = 8)
        {
            Random random = new Random();
            List<char> result = new List<char>();
            var maxIdx = CharsPool.Value.Count;
            result.Add(CharsPool.Value.ElementAt(random.Next(11, maxIdx)));

            for (int idx = 0; idx < length - 1; idx++)
            {
                result.Add(CharsPool.Value.ElementAt(random.Next(0, maxIdx)));
            }

            return string.Join("", result);
        }

        [TestCleanup]
        public void Clean()
        {
            try
            {
                var appList = this.account.ListApplicationsAsync(CancellationToken.None).Result;
                appList.Where(a => a.Name.StartsWith(this.TestAppPrefix))
                    .AsParallel()
                    .ForAll(app => app.DeleteIfExistsAsync(CancellationToken.None).Wait());

            }
            catch (Exception ex)
            {
                // log the exception.
            }
        }

    }
}
