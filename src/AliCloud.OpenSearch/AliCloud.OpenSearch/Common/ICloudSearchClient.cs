﻿// ----------------------------------------------------------------------
// <copyright file="ICloudSearchClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    /// <summary>
    /// Class CloudSearchClient.
    /// </summary>
    internal class CloudSearchClient
    {
        /// <summary>
        /// The HTTP client
        /// </summary>
        protected readonly CloudSearchHttpClient httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        internal CloudSearchClient(CloudSearchHttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
    }
}
