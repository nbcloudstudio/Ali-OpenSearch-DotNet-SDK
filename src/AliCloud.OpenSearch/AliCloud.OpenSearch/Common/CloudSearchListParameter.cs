﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchListParameter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class CloudSearchListParameter.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class CloudSearchListParameter<T> : CloudSearchStringParameter
    {
        /// <summary>
        /// The items
        /// </summary>
        private readonly IEnumerable<T> items;
        /// <summary>
        /// The separator
        /// </summary>
        private readonly string separator;
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchListParameter{T}"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="items">The items.</param>
        /// <param name="separator">The separator.</param>
        public CloudSearchListParameter(string name, IEnumerable<T> items, string separator)
            : base(name, string.Empty)
        {
            this.items = items;
            this.separator = separator;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public override string Value
        {
            get
            {
                if (this.items == null)
                {
                    return null;
                }

                return string.Join(this.separator, this.items.Select(p => Convert.ToString(p)).ToArray());
            }
        }
    }
}
