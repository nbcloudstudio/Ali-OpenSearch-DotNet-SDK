﻿// ----------------------------------------------------------------------
// <copyright file="ActionThresholdManager.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Class ActionThresholdManager.
    /// </summary>
    internal static class ActionThresholdManager
    {
        /// <summary>
        /// The action execute dictionary
        /// </summary>
        private static ConcurrentDictionary<string, ActionExecutionCounter> ActionExecuteDict = new ConcurrentDictionary<string, ActionExecutionCounter>();

        /// <summary>
        /// wait as an asynchronous operation.
        /// </summary>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="frequence">The frequence.</param>
        /// <param name="durationInSeconds">The duration in seconds.</param>
        /// <returns>Task.</returns>
        public static async Task WaitAsync(string actionName, int frequence, int durationInSeconds)
        {
            var executor = ActionExecuteDict.GetOrAdd(actionName, new ActionExecutionCounter(TimeSpan.FromSeconds(durationInSeconds).Ticks, actionName));
            if (executor.GetExecutionCountInPeriod() >= frequence)
            {
                await Task.Delay(TimeSpan.FromSeconds(durationInSeconds));
            } 
            
            executor.OnExecute();             
        }

        /// <summary>
        /// Class ActionExecutionCounter.
        /// </summary>
        private class ActionExecutionCounter
        {
            /// <summary>
            /// The period in ticks
            /// </summary>
            private readonly long periodInTicks;
            /// <summary>
            /// The action name
            /// </summary>
            private readonly string actionName;

            /// <summary>
            /// The last execute time in ticks
            /// </summary>
            private long lastExecuteTimeInTicks = DateTime.Now.Ticks;
            /// <summary>
            /// The execution count in period
            /// </summary>
            private long executionCountInPeriod = 0;
            /// <summary>
            /// The last logged time in ticks
            /// </summary>
            private long lastLoggedTimeInTicks = 0;

            /// <summary>
            /// Initializes a new instance of the <see cref="ActionExecutionCounter"/> class.
            /// </summary>
            /// <param name="periodInTicks">The period in ticks.</param>
            /// <param name="actionName">Name of the action.</param>
            public ActionExecutionCounter(long periodInTicks, string actionName)
            {
                this.periodInTicks = periodInTicks;
                this.actionName = actionName;
            }

            /// <summary>
            /// Called when [execute].
            /// </summary>
            public void OnExecute()
            {
                var nowTicks = DateTime.Now.Ticks;
                var expectedLastRequestTimeInTicks = this.lastExecuteTimeInTicks;
                if (nowTicks - expectedLastRequestTimeInTicks > 0)
                {
                    var exchangeTimeInTicks = Interlocked.CompareExchange(
                        ref this.lastExecuteTimeInTicks,
                        nowTicks,
                        expectedLastRequestTimeInTicks);

                    if (expectedLastRequestTimeInTicks == exchangeTimeInTicks)
                    {
                        var executionCountInPeriod = this.executionCountInPeriod;
                        if (executionCountInPeriod < 0)
                        {
                            Interlocked.Exchange(ref this.executionCountInPeriod, 0);
                        }
                        else
                        {
                            var timeDiff = Math.Min(nowTicks - expectedLastRequestTimeInTicks, this.periodInTicks);
                            var removedRequests = (long)((double)timeDiff / this.periodInTicks * executionCountInPeriod);

                            Interlocked.Add(ref this.executionCountInPeriod, -removedRequests);
                        }
                    }
                }

                Interlocked.Add(ref this.executionCountInPeriod, 1);

                // log the execution count.
                long expectedLoggedTimeInTicks = this.lastLoggedTimeInTicks;
                if (nowTicks - expectedLoggedTimeInTicks > this.periodInTicks)
                {
                    if (expectedLoggedTimeInTicks ==
                        Interlocked.CompareExchange(ref this.lastLoggedTimeInTicks, nowTicks, expectedLoggedTimeInTicks))
                    {
                        long executionCount = this.GetExecutionCountInPeriod();
                    }
                }
            }

            /// <summary>
            /// Get execution count in the specified period.
            /// </summary>
            /// <returns>The action's execution count in a period of time.</returns>
            public long GetExecutionCountInPeriod()
            {
                if (DateTime.Now.Ticks - this.lastExecuteTimeInTicks > this.periodInTicks)
                {
                    return 0;
                }
                else
                {
                    return Math.Max(0, this.executionCountInPeriod);
                }
            }

            /// <summary>
            /// Gets the last logged time in ticks.
            /// </summary>
            /// <value>The last logged time in ticks.</value>
            public long LastLoggedTimeInTicks
            {
                get
                {
                    return this.lastLoggedTimeInTicks;
                }
            }
        }
    }
}
