﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchCollectionParameter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class CloudSearchCollectionParameter.
    /// </summary>
    internal class CloudSearchCollectionParameter : CloudSearchStringParameter
    {
        /// <summary>
        /// The parameters
        /// </summary>
        private ICollection<ICloudSearchParameter> parameters = new List<ICloudSearchParameter>();

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchCollectionParameter"/> class.
        /// </summary>
        /// <param name="separator">The separator.</param>
        /// <param name="joinSymbol">The join symbol.</param>
        public CloudSearchCollectionParameter(string separator, string joinSymbol = "=")
            : base(string.Empty, string.Empty)
        {
            this.Separator = separator;
            this.JoinSymbol = joinSymbol;
        }

        /// <summary>
        /// Gets the separator.
        /// </summary>
        /// <value>The separator.</value>
        public string Separator { get; private set; }

        /// <summary>
        /// Gets the join symbol.
        /// </summary>
        /// <value>The join symbol.</value>
        public string JoinSymbol { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public override string Value
        {
            get
            {
                return string.Join(
                    this.Separator,
                    parameters
                        .Where(p => !p.IsNullOrEmpty())
                        .Select(p => "{0}{1}{2}".InvariantFormat(p.Name, this.JoinSymbol, p.Value)).ToArray());
            }
        }

        /// <summary>
        /// Adds the parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        public void AddParameter(ICloudSearchParameter parameter)
        {
            this.parameters.Add(parameter);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.Value;
        }
    }
}
