﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchHttpClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Auth;
    using AliCloud.OpenSearch.Exceptions;
    using AliCloud.OpenSearch.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchHttpClient.
    /// </summary>
    internal class CloudSearchHttpClient
    {
        /// <summary>
        /// The version
        /// </summary>
        private const string Version = "v2";
        /// <summary>
        /// The signature method
        /// </summary>
        private const string SignatureMethod = "HMAC-SHA1";
        /// <summary>
        /// The signature version
        /// </summary>
        private const string SignatureVersion = "1.0";

        /// <summary>
        /// The host
        /// </summary>
        private readonly string host;
        /// <summary>
        /// The credential
        /// </summary>
        private readonly CloudSearchCredential credential;
        /// <summary>
        /// The share parameters
        /// </summary>
        private readonly ICollection<ICloudSearchParameter> shareParameters;

        // TODO: do we need a useHttps choice?
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchHttpClient"/> class.
        /// </summary>
        /// <param name="credential">The credential.</param>
        /// <param name="host">The host.</param>
        public CloudSearchHttpClient(CloudSearchCredential credential, string host)
        {
            this.credential = credential;
            this.host = host;
            this.shareParameters = new List<ICloudSearchParameter>();
            this.shareParameters.Add(new CloudSearchStringParameter("Version", Version));
            this.shareParameters.Add(new CloudSearchStringParameter("AccessKeyId", this.credential.ClientId));
            this.shareParameters.Add(new CloudSearchStringParameter("SignatureMethod", SignatureMethod));
            this.shareParameters.Add(new CloudSearchStringParameter("SignatureVersion", SignatureVersion));
        }

        /// <summary>
        /// Sends the asynchronous.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task&lt;CloudSearchResponse&gt;.</returns>
        public Task<CloudSearchResponse> SendAsync(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            return this.SendAsync<CloudSearchResponse>(method, path, parameters, token, retryPolicy);
        }

        /// <summary>
        /// send as an asynchronous operation.
        /// </summary>
        /// <typeparam name="TResponse">The type of the t response.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task&lt;TResponse&gt;.</returns>
        /// <exception cref="System.Threading.Tasks.TaskCanceledException"></exception>
        public async Task<TResponse> SendAsync<TResponse>(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null) where TResponse : CloudSearchResponse
        {
            if (token.IsCancellationRequested)
            {
                throw new TaskCanceledException();
            }

            TResponse result = default(TResponse);
            Exception ex = null;

            CloudSearchRetryPolicy policy = new CloudSearchRetryPolicy(retryPolicy);

            // Increment the retry times for the first time request.
            policy.RetryTimes++;
            while (await policy.RetryAvaliable(result, ex))
            {
                try
                {
                    ex = null;
                    HttpRequestMessage message = this.CreateRequestMessage(method, path, parameters);
                    using (HttpClient client = this.CreateHttpClient())
                    {
                        Debug.WriteLine("========DEBUG REQUEST======");
                        Debug.WriteLine(message.RequestUri);
                        var response = await client.SendAsync(message, token);
                        result = await this.ParseResponse<TResponse>(response);
                    }

                    if (result != null && result.Status == CloudSearchStatus.OK)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    ex = e;
                }
            }

            if (ex != null)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// Sends the asynchronous with exception.
        /// </summary>
        /// <typeparam name="TResponse">The type of the t response.</typeparam>
        /// <typeparam name="TResult">The type of the t result.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task&lt;TResult&gt;.</returns>
        /// <exception cref="AliCloud.OpenSearch.Exceptions.CloudSearchException">-1;Unknown exception</exception>
        public async Task<TResult> SendAsyncWithException<TResponse, TResult>(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null) where TResponse : CloudSearchResponse
        {
            var response = await this.SendAsync<TResponse>(method, path, parameters, token, retryPolicy);
            if (response.Status == CloudSearchStatus.OK)
            {
                return response.As<TResult>();
            }
            else
            {
                if (response.Errors != null && response.Errors.Count() > 0)
                {
                    throw CloudSearchExceptionManager.FromErrors(response.Errors);
                }
                else
                {
                    throw new CloudSearchException(-1, "Unknown exception");
                }
            }
        }

        /// <summary>
        /// Sends the asynchronous with exception.
        /// </summary>
        /// <typeparam name="TResult">The type of the t result.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task&lt;TResult&gt;.</returns>
        public Task<TResult> SendAsyncWithException<TResult>(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            return this.SendAsyncWithException<CloudSearchResponse, TResult>(method, path, parameters, token, retryPolicy);
        }

        /// <summary>
        /// Sends the asynchronous with exception.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        public async Task SendAsyncWithException(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            await this.SendAsyncWithException<JToken>(method, path, parameters, token, retryPolicy);
        }

        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <typeparam name="TResponse">The type of the t response.</typeparam>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task&lt;TResponse&gt;.</returns>
        public Task<TResponse> GetAsync<TResponse>(string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null) where TResponse : CloudSearchResponse
        {
            return this.SendAsync<TResponse>(HttpMethod.Get, path, parameters, token, retryPolicy);
        }

        /// <summary>
        /// Gets the asynchronous with exception.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;T&gt;.</returns>
        public Task<T> GetAsyncWithException<T>(string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token)
        {
            return this.SendAsyncWithException<T>(HttpMethod.Get, path, parameters, token);
        }

        /// <summary>
        /// Gets the asynchronous with exception.
        /// </summary>
        /// <typeparam name="TResponse">The type of the t response.</typeparam>
        /// <typeparam name="TResult">The type of the t result.</typeparam>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;TResult&gt;.</returns>
        public Task<TResult> GetAsyncWithException<TResponse, TResult>(string path, ICollection<ICloudSearchParameter> parameters, CancellationToken token) where TResponse : CloudSearchResponse
        {
            return this.SendAsyncWithException<TResponse, TResult>(HttpMethod.Get, path, parameters, token);
        }

        /// <summary>
        /// Creates the HTTP client.
        /// </summary>
        /// <returns>HttpClient.</returns>
        private HttpClient CreateHttpClient()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.UseDefaultCredentials = true;
            handler.ClientCertificateOptions = ClientCertificateOption.Automatic;
            handler.AutomaticDecompression = DecompressionMethods.GZip;

            HttpClient client = new HttpClient(handler);
            client.Timeout = TimeSpan.FromSeconds(100);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

            return client;
        }

        /// <summary>
        /// Creates the request message.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>HttpRequestMessage.</returns>
        private HttpRequestMessage CreateRequestMessage(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, this.BuildUri(method, path, method == HttpMethod.Post ? null : parameters));
            // clear headers.
            request.Headers.Clear();
            request.Headers.ExpectContinue = false;

            // request.Headers.UserAgent.ParseAdd(String.Format("opensearch/.net sdk {0}", "3.0.0"));

            if (method == HttpMethod.Post)
            {
                string content = this.BuildRequestContent(method, parameters);

                // content = Regex.Replace(content, "%[0-9A-F][0-9A-F]", c => c.Value.ToLower());
                request.Content = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded");
            }

            // todo: set the other properties of the request message.
            return request;
        }

        /// <summary>
        /// Builds the URI.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="path">The path.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>Uri.</returns>
        private Uri BuildUri(HttpMethod method, string path, ICollection<ICloudSearchParameter> parameters)
        {
            string queryString = null;
            if (parameters != null)
            {

                queryString = "?" + this.BuildRequestContent(method, parameters);
            }

            // todo: check if we always use http.
            return new Uri("http://{0}{1}{2}".InvariantFormat(this.host, path, queryString));
        }

        /// <summary>
        /// Builds the content of the request.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>System.String.</returns>
        private string BuildRequestContent(HttpMethod method, ICollection<ICloudSearchParameter> parameters)
        {
            var totalParameters = parameters
                .Where(p => !p.IsNullOrEmpty()) // skip the empty parameters
                .Concat(this.shareParameters).ToList();

            totalParameters.Add(new CloudSearchStringParameter("Timestamp", this.CreateTimestamp()));
            totalParameters.Add(new CloudSearchStringParameter("SignatureNonce", this.CreateSignatureNonce()));
            var signature = this.BuildSignature(method, totalParameters);
            totalParameters.Add(new CloudSearchStringParameter("Signature", signature));

            var pairs = totalParameters
                .Where(p => !string.IsNullOrEmpty(p.Value))
                .Select(p =>
                {
                    return "{0}={1}".InvariantFormat(p.Name, Uri.EscapeDataString(p.Value));
                });

            return string.Join("&", pairs.ToArray());
        }

        /// <summary>
        /// Builds the signature.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>System.String.</returns>
        private string BuildSignature(HttpMethod method, ICollection<ICloudSearchParameter> parameters)
        {

            string canonicalizedQueryString = string.Join(
                "&",
                parameters
                // .Where(p => p.Name != "items")
                    .OrderBy(p => p.Name, StringComparer.Ordinal)
                    .Select(p => "{0}={1}".InvariantFormat(Uri.EscapeDataString(p.Name), Uri.EscapeDataString(p.Value)))
                    .ToArray());

            string stringToSign = "{0}&%2F&{1}".InvariantFormat(method, Uri.EscapeDataString(canonicalizedQueryString));

            return this.GetHmacHash(stringToSign, this.credential.SecretId + "&");
        }

        /// <summary>
        /// Parses the response.
        /// </summary>
        /// <typeparam name="TResponse">The type of the t response.</typeparam>
        /// <param name="response">The response.</param>
        /// <returns>Task&lt;TResponse&gt;.</returns>
        private async Task<TResponse> ParseResponse<TResponse>(HttpResponseMessage response) where TResponse : CloudSearchResponse
        {
            // todo: check the response http status by refer: http://develop.aliyun.com/api?spm=5176.1970902.101.1.PGXF4X

            string content = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("========DEBUG RESPONSE======");
            Debug.WriteLine(content);
            return JsonConvert.DeserializeObject<TResponse>(content);
        }

        /// <summary>
        /// Creates the signature nonce.
        /// </summary>
        /// <returns>System.String.</returns>
        private string CreateSignatureNonce()
        {
            Random random = new Random();
            return "{0:F0}{1}".InvariantFormat((DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds, random.Next(1000, 9999));
        }

        /// <summary>
        /// Creates the timestamp.
        /// </summary>
        /// <returns>System.String.</returns>
        private string CreateTimestamp()
        {
            return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        /// <summary>
        /// Gets the hmac hash.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        private string GetHmacHash(string text, string key)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(text);
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            var hmac = new HMACSHA1(byteKey);
            using (var stream = new CryptoStream(Stream.Null, hmac, CryptoStreamMode.Write))
            {
                stream.Write(byteData, 0, byteData.Length);
                stream.Close();
                return Convert.ToBase64String(hmac.Hash);
            }
        }
    }
}
