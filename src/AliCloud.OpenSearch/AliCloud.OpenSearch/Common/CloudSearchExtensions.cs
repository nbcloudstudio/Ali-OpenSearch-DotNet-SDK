﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchExtensions.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System.Globalization;
    /// <summary>
    /// Class CloudSearchExtensions.
    /// </summary>
    public static class CloudSearchExtensions
    {
        /// <summary>
        /// Invariants the format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>System.String.</returns>
        public static string InvariantFormat(this string format, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, format, args);
        }
    }
}
