﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchErrorCodes.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    /// <summary>
    /// Class CloudSearchErrorCodes.
    /// </summary>
    public static class CloudSearchErrorCodes
    {
        /// <summary>
        /// The too frequent
        /// </summary>
        public const int TooFrequent = 3007;
        /// <summary>
        /// The application not exists
        /// </summary>
        public const int AppNotExists = 2001;
        /// <summary>
        /// The application already exists
        /// </summary>
        public const int AppAlreadyExists = 2002;
        /// <summary>
        /// The application count overflow
        /// </summary>
        public const int AppCountOverflow = 2003;
        /// <summary>
        /// The illegal application name
        /// </summary>
        public const int IllegalAppName = 2004;
        /// <summary>
        /// The application not specified
        /// </summary>
        public const int AppNotSpecified = 2005;
    }
}
