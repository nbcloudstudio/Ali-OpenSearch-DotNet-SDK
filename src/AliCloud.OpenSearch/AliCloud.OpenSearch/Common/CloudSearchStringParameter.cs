﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchStringParameter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class CloudSearchStringParameter.
    /// </summary>
    internal class CloudSearchStringParameter : ICloudSearchParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchStringParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public CloudSearchStringParameter(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchStringParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public CloudSearchStringParameter(string name, object value)
            : this(name, Convert.ToString(value))
        {
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public virtual string Value { get; private set; }

        /// <summary>
        /// Determines whether [is null or empty].
        /// </summary>
        /// <returns><c>true</c> if [is null or empty]; otherwise, <c>false</c>.</returns>
        public bool IsNullOrEmpty()
        {
            return string.IsNullOrEmpty(this.Value);
        }
    }
}
