﻿// ----------------------------------------------------------------------
// <copyright file="ValidationUtility.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using System;

    /// <summary>
    /// Class ValidationUtility.
    /// </summary>
    public static class ValidationUtility
    {
        /// <summary>
        /// The argument out of range error
        /// </summary>
        private const string ArgumentOutOfRangeError = "Value out of range, valid value is between {0} and {1}.";
        /// <summary>
        /// The object is null or empty error
        /// </summary>
        private const string ObjectIsNullOrEmptyError = "{0} cannot be null or empty.";
        /// <summary>
        /// Asserts the range.
        /// </summary>
        /// <param name="paramName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static void AssertRange(string paramName, int value, int start, int end)
        {
            if (value < start || value > end)
            {
                throw new ArgumentOutOfRangeException(paramName, ArgumentOutOfRangeError.InvariantFormat(start, end));
            }
        }

        /// <summary>
        /// Asserts the not null or empty.
        /// </summary>
        /// <param name="paramName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static void AssertNotNullOrEmpty(string paramName, object value)
        {
            if (string.IsNullOrEmpty(Convert.ToString(value)))
            {
                throw new ArgumentNullException(ObjectIsNullOrEmptyError.InvariantFormat(paramName));
            }
        }
    }
}
