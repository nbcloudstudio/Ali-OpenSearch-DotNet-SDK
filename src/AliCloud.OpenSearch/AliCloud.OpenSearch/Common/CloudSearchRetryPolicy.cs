﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchRetryPolicy.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    using AliCloud.OpenSearch.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Class CloudSearchRetryPolicy.
    /// </summary>
    public class CloudSearchRetryPolicy
    {
        /// <summary>
        /// The times
        /// </summary>
        private int times = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchRetryPolicy"/> class.
        /// </summary>
        public CloudSearchRetryPolicy()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchRetryPolicy"/> class.
        /// </summary>
        /// <param name="policy">The policy.</param>
        public CloudSearchRetryPolicy(CloudSearchRetryPolicy policy)
            : this()
        {
            if (policy != null)
            {
                this.RetryCondition = policy.RetryCondition;
                this.RetryInterval = policy.RetryInterval;
                this.RetryOnException = policy.RetryOnException;
                this.RetryTimes = policy.RetryTimes;
            }
        }

        /// <summary>
        /// Gets or sets the retry condition.
        /// </summary>
        /// <value>The retry condition.</value>
        public Func<CloudSearchResponse, bool> RetryCondition { get; set; }

        /// <summary>
        /// Gets or sets the retry on exception.
        /// </summary>
        /// <value>The retry on exception.</value>
        public Func<Exception, bool> RetryOnException { get; set; }

        /// <summary>
        /// Gets or sets the retry times.
        /// </summary>
        /// <value>The retry times.</value>
        public int RetryTimes { get; set; }

        /// <summary>
        /// Gets or sets the retry interval.
        /// </summary>
        /// <value>The retry interval.</value>
        public TimeSpan RetryInterval { get; set; }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>CloudSearchRetryPolicy.</returns>
        public CloudSearchRetryPolicy Clone()
        {
            return new CloudSearchRetryPolicy(this);
        }

        /// <summary>
        /// Retries the avaliable.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="ex">The ex.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        public async Task<bool> RetryAvaliable(CloudSearchResponse response, Exception ex)
        {
            if (this.times >= this.RetryTimes)
            {
                // todo: log retry times.
                return false;
            }

            if (response != null && this.RetryCondition != null && !this.RetryCondition(response))
            {
                // todo: log retry by condition
                return false;
            }
            else if (ex != null && this.RetryOnException != null && !this.RetryOnException(ex))
            {
                // log retry by exception
                return false;
            }

            await Task.Delay(this.RetryInterval);
            Interlocked.Increment(ref this.times);
            return true;
        }
    }
}
