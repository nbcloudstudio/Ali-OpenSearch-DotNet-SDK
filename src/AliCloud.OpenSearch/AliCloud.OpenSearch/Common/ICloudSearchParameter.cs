﻿// ----------------------------------------------------------------------
// <copyright file="ICloudSearchParameter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Common
{
    /// <summary>
    /// Interface ICloudSearchParameter
    /// </summary>
    internal interface ICloudSearchParameter
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        string Value { get; }

        /// <summary>
        /// Determines whether [is null or empty].
        /// </summary>
        /// <returns><c>true</c> if [is null or empty]; otherwise, <c>false</c>.</returns>
        bool IsNullOrEmpty();
    }
}
