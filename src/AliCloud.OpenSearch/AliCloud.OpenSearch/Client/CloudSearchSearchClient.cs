﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSearchClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model.Search;

    /// <summary>
    /// Class CloudSearchSearchClient. This class cannot be inherited.
    /// </summary>
    internal sealed class CloudSearchSearchClient : CloudSearchClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchSearchClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public CloudSearchSearchClient(CloudSearchHttpClient httpClient)
            : base(httpClient)
        {
        }

        /// <summary>
        /// Searches the asynchronous.
        /// </summary>
        /// <param name="indexNames">The index names.</param>
        /// <param name="option">The option.</param>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="filterExpression">The filter expression.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="aggregateExpression">The aggregate expression.</param>
        /// <param name="distinctExpression">The distinct expression.</param>
        /// <param name="kvpairExpression">The kvpair expression.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchResult&gt;.</returns>
        public Task<CloudSearchResult> SearchAsync(
            string[] indexNames,
            CloudSearchOption option,
            SearchExpression queryExpression,
            SearchExpression filterExpression,
            SearchExpression sortExpression,
            SearchExpression aggregateExpression,
            SearchExpression distinctExpression,
            SearchExpression kvpairExpression,
            CloudSearchConfiguration configuration,
            CancellationToken token)
        {
            CloudSearchCollectionParameter queryParameter = new CloudSearchCollectionParameter("&&");
            queryParameter.AddParameter(new CloudSearchStringParameter("config", configuration));
            queryParameter.AddParameter(new CloudSearchStringParameter("query", queryExpression));
            queryParameter.AddParameter(new CloudSearchStringParameter("sort", sortExpression));
            queryParameter.AddParameter(new CloudSearchStringParameter("filter", filterExpression));
            queryParameter.AddParameter(new CloudSearchStringParameter("distinct", distinctExpression));
            queryParameter.AddParameter(new CloudSearchStringParameter("aggregate", aggregateExpression));
            queryParameter.AddParameter(new CloudSearchStringParameter("kvpairs", kvpairExpression));

            return this.SearchAsync(indexNames, option, queryParameter.ToString(), token);
        }

        public Task<CloudSearchResult> SearchAsync(
            string[] indexNames,
            CloudSearchOption option,
            SearchExpression queryString,
            CancellationToken token)
        {
            string path = "/search";
            List<ICloudSearchParameter> parameters = new List<ICloudSearchParameter>();
            parameters.Add(new CloudSearchStringParameter("index_name", string.Join(";", indexNames)));
            parameters.Add(new CloudSearchListParameter<string>("fetch_fields", option.FetchFields, ";"));
            parameters.Add(new CloudSearchListParameter<string>("qp", option.QP, ","));
            parameters.Add(new CloudSearchStringParameter("disable", option.Disable));
            parameters.Add(new CloudSearchStringParameter("first_formula_name", option.FirstFormulaName));
            parameters.Add(new CloudSearchStringParameter("formula_name", option.FirstFormulaName));
            parameters.Add(new CloudSearchStringParameter("summary", option.Summary));

            parameters.Add(new CloudSearchStringParameter("query", queryString));

            return this.httpClient.GetAsyncWithException<CloudSearchResult>(path, parameters, token);
        }
    }
}
