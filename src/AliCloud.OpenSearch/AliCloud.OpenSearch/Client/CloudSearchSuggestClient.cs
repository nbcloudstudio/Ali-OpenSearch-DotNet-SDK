﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSuggestClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model;

    /// <summary>
    /// Class CloudSearchSuggestionClient. This class cannot be inherited.
    /// </summary>
    internal sealed class CloudSearchSuggestionClient : CloudSearchClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchSuggestionClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public CloudSearchSuggestionClient(CloudSearchHttpClient httpClient)
            : base(httpClient)
        {
        }

        /// <summary>
        /// Gets the suggestion list asynchronous.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="suggestName">Name of the suggest.</param>
        /// <param name="hit">The hit.</param>
        /// <param name="query">The query.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;IEnumerable&lt;CloudSearchSuggestion&gt;&gt;.</returns>
        public Task<IEnumerable<CloudSearchSuggestion>> GetSuggestionListAsync(string indexName, string suggestName, int hit, string query, CancellationToken token)
        {
            string relativeUrl = "/suggest";
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("index_name", indexName),
                new CloudSearchStringParameter("suggest_name", suggestName),
                new CloudSearchStringParameter("hit", hit),
                new CloudSearchStringParameter("query", query),
            };

            return this.httpClient.GetAsyncWithException<CloudSearchSuggestResponse, IEnumerable<CloudSearchSuggestion>>(relativeUrl, parameters, token);
        }
    }
}
