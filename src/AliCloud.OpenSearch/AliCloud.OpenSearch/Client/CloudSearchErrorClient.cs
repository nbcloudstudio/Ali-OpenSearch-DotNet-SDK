﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchErrorClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Client.Error.Result;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model;

    /// <summary>
    /// Class CloudSearchErrorClient. This class cannot be inherited.
    /// </summary>
    internal sealed class CloudSearchErrorClient : CloudSearchClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchErrorClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public CloudSearchErrorClient(CloudSearchHttpClient httpClient)
            : base(httpClient)
        {
        }

        /// <summary>
        /// Gets the error logs asynchronous.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortMode">The sort mode.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchApplicationErrorLog&gt;.</returns>
        public Task<CloudSearchApplicationErrorLog> GetErrorLogsAsync(string appName, int page, int pageSize, CloudSearchSortMode sortMode, CancellationToken token)
        {
            string relativeUrl = string.Format("/index/error/{0}", appName);
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("page", page),
                new CloudSearchStringParameter("page_size", pageSize),
                new CloudSearchStringParameter("sortMode", sortMode.ToString())
            };

            return this.httpClient.GetAsyncWithException<CloudSearchListResponse, CloudSearchApplicationErrorLog>(relativeUrl, parameters, token);
        }
    }
}
