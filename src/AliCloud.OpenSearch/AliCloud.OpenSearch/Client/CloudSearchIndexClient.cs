﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchIndexClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model;

    /// <summary>
    /// Class CloudSearchIndexClient. This class cannot be inherited.
    /// </summary>
    internal sealed class CloudSearchIndexClient : CloudSearchClient
    {
        /// <summary>
        /// Default page size of list applications
        /// </summary>
        private const int DefaultPageSize = 1000;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchIndexClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public CloudSearchIndexClient(CloudSearchHttpClient httpClient)
            : base(httpClient)
        {
        }

        /// <summary>
        /// Creates the index asynchronous.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="description">The description.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task CreateIndexAsync(string indexName, string templateName, string description, CancellationToken token)
        {
            string relativeUrl = string.Format("/index/{0}", indexName);
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("action", "create"),
                new CloudSearchStringParameter("template", templateName),
                new CloudSearchStringParameter("index_desc",description)
            };

            return this.httpClient.SendAsyncWithException(HttpMethod.Post, relativeUrl, parameters, token);
        }

        /// <summary>
        /// delete index as an asynchronous operation.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public async Task DeleteIndexAsync(string indexName, CancellationToken token)
        {
            string relativeUrl = string.Format("/index/{0}", indexName);
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("action", "delete")
            };

            await this.httpClient.GetAsyncWithException<int>(relativeUrl, parameters, token);
        }

        /// <summary>
        /// Gets the index status asynchronous.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchApplicationStatus&gt;.</returns>
        public Task<CloudSearchApplicationStatus> GetIndexStatusAsync(string indexName, CancellationToken token)
        {
            string relativeUrl = string.Format("/index/{0}", indexName);
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("action", "status")
            };

            return this.httpClient.GetAsyncWithException<CloudSearchApplicationStatus>(relativeUrl, parameters, token);
        }

        /// <summary>
        /// get index list segment as an asynchronous operation.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchApplicationListSegment&gt;.</returns>
        public async Task<CloudSearchApplicationListSegment> GetIndexListSegmentAsync(int page, int pageSize, CancellationToken token)
        {
            string path = "/index";
            var parameters = new ICloudSearchParameter[]
            {
                new CloudSearchStringParameter("page", page),
                new CloudSearchStringParameter("page_size", pageSize)
            };

            var respones = await this.httpClient.GetAsync<CloudSearchListResponse>(path, parameters, token);

            var applications = respones.As<IEnumerable<CloudSearchApplication>>();

            return new CloudSearchApplicationListSegment()
            {
                Applications = applications ?? new List<CloudSearchApplication>(),
                Total = respones.Total,
                Page = page,
                PageSize = pageSize
            };
        }

        /// <summary>
        /// get index list as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;IEnumerable&lt;CloudSearchApplication&gt;&gt;.</returns>
        public async Task<IEnumerable<CloudSearchApplication>> GetIndexListAsync(CancellationToken token)
        {
            var applications = new List<CloudSearchApplication>();
            int page = 0;
            CloudSearchApplicationListSegment segment = null;
            do
            {
                page++;
                segment = await this.GetIndexListSegmentAsync(page, DefaultPageSize, token);
                applications.AddRange(segment.Applications);
            } while (applications.Count < segment.Total);

            return applications;
        }

        /// <summary>
        /// Rebuilds the index with table import asynchronous.
        /// </summary>
        /// <param name="indexName">Name of the index.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchTask&gt;.</returns>
        public Task<CloudSearchTask> RebuildIndexAsync(string indexName, string tableName, CancellationToken token)
        {
            string relativeUrl = string.Format("/index/{0}", indexName);
            var parameters = new List<ICloudSearchParameter>
            {
                new CloudSearchStringParameter("action", "createtask")
            };

            if (!string.IsNullOrEmpty(tableName))
            {
                parameters.Add(new CloudSearchStringParameter("operate", "import"));
                parameters.Add(new CloudSearchStringParameter("table_name", tableName));
            }

            return this.httpClient.GetAsyncWithException<CloudSearchTask>(relativeUrl, parameters, token);
        }
    }
}
