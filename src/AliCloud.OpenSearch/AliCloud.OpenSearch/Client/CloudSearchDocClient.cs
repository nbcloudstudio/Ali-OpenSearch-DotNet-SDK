﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchDocClient.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Threading.Tasks.Dataflow;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchDocClient. This class cannot be inherited.
    /// </summary>
    internal sealed class CloudSearchDocClient : CloudSearchClient
    {
        // For each push action, the size of the package is at most 2MB,
        // refer: http://help.opensearch.aliyun.com/index.php?title=%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86API#.E4.B8.8A.E4.BC.A0.E6.96.87.E6.A1.A3
        /// <summary>
        /// The maximum buffer size
        /// </summary>
        private const int MaxBufferSize = 2 * 1024 * 1024;

        // The function can execute at most 5 times per seconds 
        // by refer: http://help.opensearch.aliyun.com/index.php?title=%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86API#.E4.B8.8A.E4.BC.A0.E6.96.87.E6.A1.A3
        /// <summary>
        /// The threshold in seconds
        /// </summary>
        private const int ThresholdInSeconds = 5;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchDocClient"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public CloudSearchDocClient(CloudSearchHttpClient httpClient)
            : base(httpClient)
        {
        }

        /// <summary>
        /// Uploads the size of the json string without check.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="json">The json.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        internal async Task UploadJsonStringWithoutCheckSize(string appName, string tableName, string json, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            string actionName = "{0}-{1}".InvariantFormat(appName, "uploaddoc");
            string path = "/index/doc/{0}".InvariantFormat(appName);

            await ActionThresholdManager.WaitAsync(actionName, ThresholdInSeconds, 1);
            await this.httpClient.SendAsync(
               HttpMethod.Post,
               path,
               new ICloudSearchParameter[]{
                            new CloudSearchStringParameter("action","push"),
                            new CloudSearchStringParameter("items",json),
                            new CloudSearchStringParameter("table_name",tableName)
                        },
               token,
               retryPolicy);
        }

        /// <summary>
        /// Uploads the size of the json tokens without check.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        internal Task UploadJsonTokensWithoutCheckSize(string appName, string tableName, IEnumerable<JToken> raws, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            return this.UploadJsonStringWithoutCheckSize(appName, tableName, JsonConvert.SerializeObject(raws), token, retryPolicy);
        }

        /// <summary>
        /// Uploads the json tokens.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        internal async Task UploadJsonTokens(string appName, string tableName, IEnumerable<JToken> raws, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            ActionBlock<JToken[]> pushAction = new ActionBlock<JToken[]>(async items =>
            {
                await this.UploadJsonTokensWithoutCheckSize(appName, tableName, items, token, retryPolicy);
            }, new ExecutionDataflowBlockOptions()
            {
                BoundedCapacity = ThresholdInSeconds
            });

            ActionBlock<IEnumerable<JToken>> readAction = new ActionBlock<IEnumerable<JToken>>(dataRaws =>
            {
                List<JToken> buffer = new List<JToken>();
                var sbSize = 0;
                foreach (var raw in dataRaws)
                {
                    if (token.IsCancellationRequested)
                    {
                        throw new TaskCanceledException();
                    }

                    var rawStr = raw.ToString();
                    var rawSize = Encoding.Default.GetByteCount(rawStr);
                    if (rawSize + sbSize < MaxBufferSize - 1)
                    {
                        buffer.Add(raw);
                        sbSize += rawSize;
                    }
                    else
                    {
                        pushAction.Post(buffer.ToArray());
                        buffer.Clear();
                    }
                }

                if (buffer.Count > 0)
                {
                    pushAction.Post(buffer.ToArray());
                }

                pushAction.Complete();
            });

            readAction.Post(raws);
            readAction.Complete();
            await pushAction.Completion;
            await readAction.Completion;
        }

        /// <summary>
        /// Uploads the cloud search table raws.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        internal Task UploadCloudSearchTableRaws(string appName, string tableName, IEnumerable<CloudSearchTableRaw> raws, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            JArray jarry = JArray.FromObject(raws);
            return this.UploadJsonTokens(appName, tableName, jarry, token, retryPolicy);
        }

        /// <summary>
        /// upload file as an asynchronous operation.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="token">The token.</param>
        /// <param name="retryPolicy">The retry policy.</param>
        /// <returns>Task.</returns>
        public async Task UploadFileAsync(string appName, string tableName, string filePath, CancellationToken token, CloudSearchRetryPolicy retryPolicy = null)
        {
            using (StreamReader fileReader = File.OpenText(filePath))
            using (JsonTextReader reader = new JsonTextReader(fileReader))
            {
                ActionBlock<JObject[]> pushAction = new ActionBlock<JObject[]>(async items =>
                {
                    await this.UploadJsonTokensWithoutCheckSize(appName, tableName, items, token, retryPolicy);
                }, new ExecutionDataflowBlockOptions()
                {
                    BoundedCapacity = ThresholdInSeconds
                });

                ActionBlock<JsonReader> readAction = new ActionBlock<JsonReader>(jreader =>
                {
                    List<JObject> items = new List<JObject>();
                    var sbSize = 0;
                    while (jreader.Read())
                    {
                        if (token.IsCancellationRequested)
                        {
                            throw new TaskCanceledException();
                        }

                        if (jreader.TokenType == JsonToken.StartObject)
                        {
                            JObject obj = JObject.Load(jreader);
                            string objStr = obj.ToString();
                            var objSize = Encoding.Default.GetByteCount(objStr);
                            if (objSize + sbSize < MaxBufferSize - 1)
                            {
                                items.Add(obj);
                                sbSize += objSize;
                            }
                            else
                            {
                                pushAction.Post(items.ToArray());
                                items.Clear();
                            }
                        }
                    }

                    if (items.Count > 0)
                    {
                        pushAction.Post(items.ToArray());
                    }

                    pushAction.Complete();
                });

                readAction.Post(reader);
                readAction.Complete();
                await pushAction.Completion;
                await readAction.Completion;
            }
        }
    }
}
