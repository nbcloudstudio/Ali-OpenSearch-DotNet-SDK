﻿using System;
// ----------------------------------------------------------------------
// <copyright file="CloudSearchItemCollectionJsonConverter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------


namespace AliCloud.OpenSearch.JsonConverters
{
    using AliCloud.OpenSearch.Model;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchItemCollectionJsonConverter.
    /// </summary>
    internal class CloudSearchItemCollectionJsonConverter : JsonConverter
    {
        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns><c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(ICloudSearchItemCollection).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            ICloudSearchItemCollection collection = null;
            if (reader.TokenType != JsonToken.Null)
            {

                collection = existingValue as ICloudSearchItemCollection;
                if (existingValue == null)
                {
                    collection = Activator.CreateInstance(objectType) as ICloudSearchItemCollection;
                }

                CreateItem(reader, collection, serializer);
            }

            return collection;
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {

        }

        /// <summary>
        /// Creates the item.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="serializer">The serializer.</param>
        private static void CreateItem(JsonReader reader, ICloudSearchItemCollection collection, JsonSerializer serializer)
        {
            var dr = Activator.CreateInstance(collection.GetItemType());
            CheckedRead(reader);

            while (reader.TokenType == JsonToken.PropertyName)
            {
                string collectionName = (string)reader.Value;

                CheckedRead(reader);

                var item = serializer.Deserialize(reader, collection.GetItemType());
                if (item is ICloudSearchItemCollection)
                {
                    (item as ICloudSearchItemCollection).SetName(collectionName);
                }

                collection.AddItem(collectionName, item);

                CheckedRead(reader);
            }
        }

        /// <summary>
        /// Checkeds the read.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <exception cref="Newtonsoft.Json.JsonSerializationException">Unexpected end when reading DataTable.</exception>
        private static void CheckedRead(JsonReader reader)
        {
            if (!reader.Read())
            {
                throw new JsonSerializationException("Unexpected end when reading DataTable.");
            }
        }
    }
}