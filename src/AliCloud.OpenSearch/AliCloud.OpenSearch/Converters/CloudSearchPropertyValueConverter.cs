﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchPropertyValueConverter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Converters
{
    /// <summary>
    /// Class CloudSearchPropertyValueConverter.
    /// </summary>
    public abstract class CloudSearchPropertyValueConverter
    {
        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public abstract string Convert(object value);
    }

    /// <summary>
    /// Class CloudSearchPropertyValueLowerCaseConvertor.
    /// </summary>
    public class CloudSearchPropertyValueLowerCaseConvertor : CloudSearchPropertyValueConverter
    {

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public override string Convert(object value)
        {
            if (value == null)
            {
                return null;
            }

            return value.ToString().ToLowerInvariant();
        }
    }
}
