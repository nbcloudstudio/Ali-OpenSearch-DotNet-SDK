﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchAccount.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Auth;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Model;
    using AliCloud.OpenSearch.Model.Search;

    /// <summary>
    /// Class CloudSearchAccount. This class cannot be inherited.
    /// </summary>
    public sealed class CloudSearchAccount
    {
        /// <summary>
        /// The default host
        /// </summary>
        private const string DefaultHost = "opensearch.etao.com";
        /// <summary>
        /// The HTTP client
        /// </summary>
        private readonly CloudSearchHttpClient httpClient;
        /// <summary>
        /// The client pool
        /// </summary>
        private readonly ConcurrentDictionary<Type, CloudSearchClient> clientPool = new ConcurrentDictionary<Type, CloudSearchClient>();

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchAccount"/> class.
        /// </summary>
        /// <param name="credential">The credential.</param>
        /// <param name="host">The host.</param>
        public CloudSearchAccount(CloudSearchCredential credential, string host = DefaultHost)
        {
            this.httpClient = new CloudSearchHttpClient(credential, host ?? DefaultHost);
        }

        /// <summary>
        /// Gets the application referance.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <returns>CloudSearchApplication.</returns>
        public CloudSearchApplication GetApplicationReferance(string appName)
        {
            return new CloudSearchApplication(appName, this);
        }

        /// <summary>
        /// list applications as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;IEnumerable&lt;CloudSearchApplication&gt;&gt;.</returns>
        public async Task<IEnumerable<CloudSearchApplication>> ListApplicationsAsync(CancellationToken token)
        {
            var all = await this.CreateCloudSearchIndexClient()
                .GetIndexListAsync(token);

            all.AsParallel().ForAll(a => a.Account = this);

            return all;
        }

        /// <summary>
        /// Lists the applications.
        /// </summary>
        /// <returns>IEnumerable&lt;CloudSearchApplication&gt;.</returns>
        public IEnumerable<CloudSearchApplication> ListApplications()
        {
            return this.ListApplicationsAsync(CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// get application list segment as an asynchronous operation.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchApplicationListSegment&gt;.</returns>
        public async Task<CloudSearchApplicationListSegment> GetApplicationListSegmentAsync(int page, int pageSize, CancellationToken token)
        {
            var segment = await this.CreateCloudSearchIndexClient()
                .GetIndexListSegmentAsync(page, pageSize, token);

            segment.Applications.AsParallel().ForAll(a => a.Account = this);

            return segment;
        }

        /// <summary>
        /// Gets the application list segment.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>CloudSearchApplicationListSegment.</returns>
        public CloudSearchApplicationListSegment GetApplicationListSegment(int page, int pageSize)
        {
            return this.GetApplicationListSegmentAsync(page, pageSize, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Searches the asynchronous.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <param name="option">The option.</param>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchResult&gt;.</returns>
        public Task<CloudSearchResult> SearchAsync(
            string[] appNames,
            CloudSearchOption option,
            QueryExpression queryExpression,
            CloudSearchConfiguration configuration,
            CancellationToken token)
        {
            CloudSearchExpressions expressions = new CloudSearchExpressions()
            {
                QueryExpression = queryExpression
            };

            return this.SearchAsync(appNames, option, expressions, configuration, token);
        }

        /// <summary>
        /// Searches the specified application names.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <param name="option">The option.</param>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>CloudSearchResult.</returns>
        public CloudSearchResult Search(
            string[] appNames,
            CloudSearchOption option,
            QueryExpression queryExpression,
            CloudSearchConfiguration configuration)
        {
            return this.SearchAsync(appNames, option, queryExpression, configuration, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Searches the asynchronous.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <param name="option">The option.</param>
        /// <param name="expressions">The expressions.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchResult&gt;.</returns>
        public Task<CloudSearchResult> SearchAsync(
            string[] appNames,
            CloudSearchOption option,
            CloudSearchExpressions expressions,
            CloudSearchConfiguration configuration,
            CancellationToken token)
        {
            return this.CreateCloudSearchSearchClient()
                .SearchAsync(
                    appNames,
                    option,
                    expressions.QueryExpression,
                    expressions.FilterExpression,
                    expressions.SortExpression,
                    expressions.AggregateExpression,
                    expressions.DistinctExpression,
                    expressions.KeyValuePairExpression,
                    configuration,
                    token);
        }

        /// <summary>
        /// Searches the specified application names.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <param name="option">The option.</param>
        /// <param name="expressions">The expressions.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>CloudSearchResult.</returns>
        public CloudSearchResult Search(
            string[] appNames,
            CloudSearchOption option,
            CloudSearchExpressions expressions,
            CloudSearchConfiguration configuration)
        {
            return this.SearchAsync(appNames, option, expressions, configuration, CancellationToken.None)
                .Result;
        }

        public Task<CloudSearchResult> SearchByStringAsync(
            string[] appNames,
            CloudSearchOption option,
            string fullQueryString,
            CancellationToken token)
        {
            return this.CreateCloudSearchSearchClient()
                .SearchAsync(
                    appNames,
                    option,
                    fullQueryString,
                    token);
        }

        /// <summary>
        /// Searches the by string.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <param name="option">The option.</param>
        /// <param name="fullQueryString">The full query string.</param>
        /// <returns>CloudSearchResult.</returns>
        public CloudSearchResult SearchByString(
            string[] appNames,
            CloudSearchOption option,
            string fullQueryString)
        {
            return this.SearchByStringAsync(appNames, option, fullQueryString, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Creates the cloud search index client.
        /// </summary>
        /// <returns>CloudSearchIndexClient.</returns>
        internal CloudSearchIndexClient CreateCloudSearchIndexClient()
        {
            return this.GetClient<CloudSearchIndexClient>();
        }

        /// <summary>
        /// Creates the cloud search search client.
        /// </summary>
        /// <returns>CloudSearchSearchClient.</returns>
        internal CloudSearchSearchClient CreateCloudSearchSearchClient()
        {
            return this.GetClient<CloudSearchSearchClient>();
        }

        /// <summary>
        /// Creates the cloud search suggestion client.
        /// </summary>
        /// <returns>CloudSearchSuggestionClient.</returns>
        internal CloudSearchSuggestionClient CreateCloudSearchSuggestionClient()
        {
            return this.GetClient<CloudSearchSuggestionClient>();
        }

        /// <summary>
        /// Creates the cloud search document client.
        /// </summary>
        /// <returns>CloudSearchDocClient.</returns>
        internal CloudSearchDocClient CreateCloudSearchDocClient()
        {
            return this.GetClient<CloudSearchDocClient>();
        }

        /// <summary>
        /// Creates the cloud search error client.
        /// </summary>
        /// <returns>CloudSearchErrorClient.</returns>
        internal CloudSearchErrorClient CreateCloudSearchErrorClient()
        {
            return this.GetClient<CloudSearchErrorClient>();
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <typeparam name="TClient">The type of the t client.</typeparam>
        /// <returns>TClient.</returns>
        internal TClient GetClient<TClient>() where TClient : CloudSearchClient
        {
            return this.clientPool.GetOrAdd(
                typeof(TClient),
                Activator.CreateInstance(typeof(TClient), this.httpClient) as TClient) as TClient;
        }
    }
}
