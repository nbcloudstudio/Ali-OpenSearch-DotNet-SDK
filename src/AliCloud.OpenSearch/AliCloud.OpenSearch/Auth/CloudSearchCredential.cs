﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchCredential.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Auth
{
    /// <summary>
    /// Class CloudSearchCredential.
    /// </summary>
    public class CloudSearchCredential
    {
        /// <summary>
        /// Gets the client identifier.
        /// </summary>
        /// <value>The client identifier.</value>
        public string ClientId { get; private set; }
        /// <summary>
        /// Gets the secret identifier.
        /// </summary>
        /// <value>The secret identifier.</value>
        public string SecretId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchCredential"/> class.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="secretId">The secret identifier.</param>
        public CloudSearchCredential(string clientId, string secretId)
        {
            this.ClientId = clientId;
            this.SecretId = secretId;
        }
    }
}
