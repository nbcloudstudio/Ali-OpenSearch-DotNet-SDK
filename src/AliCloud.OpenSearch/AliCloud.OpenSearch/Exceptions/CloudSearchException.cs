﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchException.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Exceptions
{
    using System;
    /// <summary>
    /// Class CloudSearchException.
    /// </summary>
    public class CloudSearchException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="message">The message.</param>
        public CloudSearchException(int errorCode, string message)
            : base(message)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>The error code.</value>
        public int ErrorCode { get; private set; }
    }
}
