﻿
// ----------------------------------------------------------------------
// <copyright file="CloudSearchExceptionManager.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AliCloud.OpenSearch.Model;

    /// <summary>
    /// Class CloudSearchExceptionManager.
    /// </summary>
    internal static class CloudSearchExceptionManager
    {
        /// <summary>
        /// Froms the error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <returns>CloudSearchException.</returns>
        public static CloudSearchException FromError(CloudSearchError error)
        {
            return new CloudSearchException(error.Code, error.Message);
        }

        /// <summary>
        /// Froms the errors.
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <returns>Exception.</returns>
        public static Exception FromErrors(IEnumerable<CloudSearchError> errors)
        {
            if (errors.Count() > 1)
            {
                return new AggregateException(errors.Select(e => new CloudSearchException(e.Code, e.Message)));
            }
            else
            {
                return FromError(errors.First());
            }
        }
    }
}
