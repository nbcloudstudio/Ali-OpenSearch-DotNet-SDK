﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchItemCollection.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class CloudSearchItemCollection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CloudSearchItemCollection<T> : ICloudSearchItemCollection where T : class
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; private set; }

        /// <summary>
        /// The items
        /// </summary>
        protected Dictionary<string, T> items = new Dictionary<string, T>();

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns>ICollection&lt;KeyValuePair&lt;System.String, T&gt;&gt;.</returns>
        public ICollection<KeyValuePair<string, T>> GetItems()
        {
            return this.items;
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns>ICollection&lt;KeyValuePair&lt;System.String, System.Object&gt;&gt;.</returns>
        ICollection<KeyValuePair<string, object>> ICloudSearchItemCollection.GetItems()
        {
            return this.items
                .Select(kv => new KeyValuePair<string, object>(kv.Key, kv.Value))
                .ToList();
        }

        /// <summary>
        /// Gets the type of the item.
        /// </summary>
        /// <returns>Type.</returns>
        Type ICloudSearchItemCollection.GetItemType()
        {
            return typeof(T);
        }

        /// <summary>
        /// Adds the item.
        /// </summary>
        /// <param name="itemName">Name of the item.</param>
        /// <param name="item">The item.</param>
        void ICloudSearchItemCollection.AddItem(string itemName, object item)
        {
            this.items.Add(itemName, item as T);
        }

        /// <summary>
        /// Sets the name.
        /// </summary>
        /// <param name="name">The name.</param>
        public void SetName(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        public IEnumerator GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get { return this.items.Count; }
        }
    }

    /// <summary>
    /// Interface ICloudSearchItemCollection
    /// </summary>
    internal interface ICloudSearchItemCollection : IEnumerable
    {
        /// <summary>
        /// Sets the name.
        /// </summary>
        /// <param name="name">The name.</param>
        void SetName(string name);

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <returns>ICollection&lt;KeyValuePair&lt;System.String, System.Object&gt;&gt;.</returns>
        ICollection<KeyValuePair<string, object>> GetItems();

        /// <summary>
        /// Gets the type of the item.
        /// </summary>
        /// <returns>Type.</returns>
        Type GetItemType();

        /// <summary>
        /// Adds the item.
        /// </summary>
        /// <param name="itemName">Name of the item.</param>
        /// <param name="item">The item.</param>
        void AddItem(string itemName, object item);
    }
}
