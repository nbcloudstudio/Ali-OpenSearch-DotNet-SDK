﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSuggestResponse.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchSuggestResponse.
    /// </summary>
    [DataContract]
    internal class CloudSearchSuggestResponse : CloudSearchResponse
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The json result.</value>
        [DataMember(Name = "suggestions")]
        public override JToken JsonResult { get; set; }
    }
}
