﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchResponse.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Cloud search http request response without any result item.
    /// </summary>
    [DataContract]
    public class CloudSearchResponse
    {
        /// <summary>
        /// Gets or sets the response status
        /// </summary>
        /// <value>The status.</value>
        [DataMember(Name = "status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CloudSearchStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the response request id
        /// </summary>
        /// <value>The request identifier.</value>
        [DataMember(Name = "request_id")]
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The json result.</value>
        [DataMember(Name = "result")]
        public virtual JToken JsonResult { get; set; }

        /// <summary>
        /// Gets or sets the response errors
        /// </summary>
        /// <value>The errors.</value>
        [DataMember(Name = "errors")]
        public IEnumerable<CloudSearchError> Errors { get; set; }

        /// <summary>
        /// Gets or sets the response tracer
        /// </summary>
        /// <value>The tracer.</value>
        [DataMember(Name = "tracer")]
        public string Tracer { get; set; }

        /// <summary>
        /// Convert the property JsonResult to .Net object
        /// </summary>
        /// <typeparam name="T">The type of .Net object</typeparam>
        /// <returns>The convert result</returns>
        public T As<T>()
        {
            if (this.JsonResult == null)
            {
                return default(T);
            }

            return this.JsonResult.ToObject<T>();
        }
    }
}
