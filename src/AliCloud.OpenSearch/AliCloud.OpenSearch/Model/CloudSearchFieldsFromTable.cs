﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchFieldsFromTable.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using AliCloud.OpenSearch.JsonConverters;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchFieldsFromTable.
    /// </summary>
    [DataContract]
    public class CloudSearchFieldsFromTable
    {
        /// <summary>
        /// Gets or sets the tables.
        /// </summary>
        /// <value>The tables.</value>
        [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
        [DataMember(Name = "tables")]
        public CloudSearchItemCollection<CloudSearchTable> Tables { get; set; }

        /// <summary>
        /// Gets or sets the master.
        /// </summary>
        /// <value>The master.</value>
        [DataMember(Name = "master")]
        public string Master { get; set; }

        /// <summary>
        /// Gets or sets the join map.
        /// </summary>
        /// <value>The join map.</value>
        [DataMember(Name = "join_map")]
        public CloudSearchTableJoinMap JoinMap { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>The level.</value>
        [DataMember(Name = "level")]
        public Dictionary<string, string> Level { get; set; }
    }
}
