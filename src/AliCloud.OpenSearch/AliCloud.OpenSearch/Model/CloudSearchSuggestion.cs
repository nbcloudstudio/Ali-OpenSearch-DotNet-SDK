﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSuggestion.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Class CloudSearchSuggestionResult.
    /// </summary>
    [DataContract]
    public class CloudSearchSuggestionResult
    {
        /// <summary>
        /// Gets or sets the search time.
        /// </summary>
        /// <value>The search time.</value>
        [DataMember(Name = "SearchTime")]
        public double SearchTime { get; set; }
        /// <summary>
        /// Gets or sets the suggestions.
        /// </summary>
        /// <value>The suggestions.</value>
        public List<CloudSearchSuggestion> Suggestions { get; set; }
    }

    /// <summary>
    /// Class CloudSearchSuggestion.
    /// </summary>
    [DataContract]
    public class CloudSearchSuggestion
    {
        /// <summary>
        /// Gets or sets the suggestion.
        /// </summary>
        /// <value>The suggestion.</value>
        [DataMember(Name = "suggestion")]
        public string Suggestion { get; set; }
    }
}
