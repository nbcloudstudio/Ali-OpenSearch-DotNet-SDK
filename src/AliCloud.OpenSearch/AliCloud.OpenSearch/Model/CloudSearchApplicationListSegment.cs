﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchApplicationListSegment.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

/// <summary>
/// The Model namespace.
/// </summary>
namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    /// <summary>
    /// Class CloudSearchApplicationListSegment.
    /// </summary>
    public class CloudSearchApplicationListSegment
    {
        /// <summary>
        /// Gets or sets the applications.
        /// </summary>
        /// <value>The applications.</value>
        public IEnumerable<CloudSearchApplication> Applications { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        public int Total { get; set; }
        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        /// <value>The page.</value>
        public int Page { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
