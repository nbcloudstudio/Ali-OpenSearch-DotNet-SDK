﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTableRaw.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchTableRaw.
    /// </summary>
    [DataContract]
    public class CloudSearchTableRaw
    {
        /// <summary>
        /// The timestamp
        /// </summary>
        private long timestamp = long.MinValue;

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>The command.</value>
        [DataMember(Name = "cmd")]
        public CloudSearchTableRawCommand Command { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        [DataMember(Name = "timestamp")]
        public long Timestamp
        {
            get
            {
                if (this.timestamp == long.MinValue)
                {
                    this.timestamp = DateTime.UtcNow.Ticks;
                }

                return this.timestamp;
            }
            set
            {
                this.timestamp = value;
            }
        }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>The fields.</value>
        [DataMember(Name = "fields")]
        public JToken Fields { get; set; }

        /// <summary>
        /// Froms the object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="command">The command.</param>
        /// <returns>CloudSearchTableRaw.</returns>
        public static CloudSearchTableRaw FromObject(object obj, CloudSearchTableRawCommand command)
        {
            var raw = new CloudSearchTableRaw();
            raw.Command = command;
            raw.Fields = JToken.FromObject(obj);
            return raw;
        }
    }
}
