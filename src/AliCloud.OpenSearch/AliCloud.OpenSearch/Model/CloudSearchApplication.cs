﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchApplication.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Threading;
    using System.Threading.Tasks;
    using AliCloud.OpenSearch.Client.Error.Result;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Exceptions;
    using AliCloud.OpenSearch.Model.Search;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchApplication.
    /// </summary>
    [DataContract]
    public class CloudSearchApplication
    {
        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <value>The account.</value>
        [JsonIgnore]
        public CloudSearchAccount Account { get; internal set; }

        // a default constructor used for json converter
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchApplication"/> class.
        /// </summary>
        internal CloudSearchApplication()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchApplication"/> class.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="account">The account.</param>
        internal CloudSearchApplication(string appName, CloudSearchAccount account)
            : this()
        {
            this.Account = account;
            this.Name = appName;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [DataMember(Name = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>The created.</value>
        [DataMember(Name = "created")]
        public string Created { get; set; }

        /// <summary>
        /// Gets or sets the name of the template.
        /// </summary>
        /// <value>The name of the template.</value>
        [DataMember(Name = "template_name")]
        public string TemplateName { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        [JsonIgnore]
        public CloudSearchApplicationStatus Status { get; set; }

        /// <summary>
        /// create if not exists as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public async Task CreateIfNotExistsAsync(CancellationToken token)
        {
            try
            {
                await this.CreateAsync(token);
            }
            catch (CloudSearchException ex)
            {
                if (ex.ErrorCode == CloudSearchErrorCodes.AppAlreadyExists)
                {
                    return;
                }

                throw;
            }
        }

        /// <summary>
        /// Creates if not exists.
        /// </summary>
        public void CreateIfNotExists()
        {
            this.CreateIfNotExistsAsync(CancellationToken.None).Wait();
        }

        /// <summary>
        /// create as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public async Task CreateAsync(CancellationToken token)
        {
            await this.Account.CreateCloudSearchIndexClient()
                    .CreateIndexAsync(this.Name, this.TemplateName, this.Description, token);
        }


        /// <summary>
        /// Creates this instance.
        /// </summary>
        public void Create()
        {
            this.CreateAsync(CancellationToken.None).Wait();
        }

        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task DeleteAsync(CancellationToken token)
        {
            return this.Account.CreateCloudSearchIndexClient()
                .DeleteIndexAsync(this.Name, token);
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Delete()
        {
            this.DeleteAsync(CancellationToken.None).Wait();
        }

        /// <summary>
        /// delete if exists as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public async Task DeleteIfExistsAsync(CancellationToken token)
        {
            try
            {
                await this.DeleteAsync(token);
            }
            catch (CloudSearchException ex)
            {
                if (ex.ErrorCode == CloudSearchErrorCodes.AppNotExists)
                {
                    return;
                }

                throw;
            }
        }

        /// <summary>
        /// Deletes if exists.
        /// </summary>
        public void DeleteIfExists()
        {
            this.DeleteIfExistsAsync(CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// fetch status as an asynchronous operation.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public async Task FetchStatusAsync(CancellationToken token)
        {
            this.Status = await this.Account.CreateCloudSearchIndexClient()
                .GetIndexStatusAsync(this.Name, token);
        }

        /// <summary>
        /// Fetches the status.
        /// </summary>
        public void FetchStatus()
        {
            this.FetchStatusAsync(CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Adds the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task AddTableRawsAsync<T>(string tableName, IEnumerable<T> raws, CancellationToken token)
        {
            var rawsToPost = raws
                .Select(raw => CloudSearchTableRaw.FromObject(raw, CloudSearchTableRawCommand.Add));

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Adds the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        public void AddTableRaws<T>(string tableName, IEnumerable<T> raws)
        {
            this.AddTableRawsAsync<T>(tableName, raws, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Adds the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task AddTableRawsAsync(string tableName, DataTable dataTable, CancellationToken token)
        {
            var jarray = JArray.FromObject(dataTable);
            var rawsToPost = jarray.Select(raw => new CloudSearchTableRaw() { Command = CloudSearchTableRawCommand.Add, Fields = raw });

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Adds the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        /// <returns>Task.</returns>
        public void AddTableRaws(string tableName, DataTable dataTable)
        {
            this.AddTableRawsAsync(tableName, dataTable, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Updates the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task UpdateTableRawsAsync<T>(string tableName, IEnumerable<T> raws, CancellationToken token)
        {
            var rawsToPost = raws
                .Select(raw => CloudSearchTableRaw.FromObject(raw, CloudSearchTableRawCommand.Update));

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Updates the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        public void UpdateTableRaws<T>(string tableName, IEnumerable<T> raws)
        {
            this.UpdateTableRawsAsync<T>(tableName, raws, CancellationToken.None).Wait();
        }

        /// <summary>
        /// Updates the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task UpdateTableRawsAsync(string tableName, DataTable dataTable, CancellationToken token)
        {
            var jarray = JArray.FromObject(dataTable);
            var rawsToPost = jarray.Select(raw => new CloudSearchTableRaw() { Command = CloudSearchTableRawCommand.Update, Fields = raw });

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Updates the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        public void UpdateTableRaws(string tableName, DataTable dataTable)
        {
            this.UpdateTableRawsAsync(tableName, dataTable, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Deletes the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task DeleteTableRawsAsync<T>(string tableName, IEnumerable<T> raws, CancellationToken token)
        {
            var rawsToPost = raws
                .AsParallel()
                .Select(raw => CloudSearchTableRaw.FromObject(raw, CloudSearchTableRawCommand.Delete));

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Deletes the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        public void DeleteTableRaws<T>(string tableName, IEnumerable<T> raws)
        {
            this.DeleteTableRawsAsync<T>(tableName, raws, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Deletes the table raws.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        /// <param name="token">The token.</param>
        /// <returns>System.Threading.Tasks.Task.</returns>
        public Task DeleteTableRawsAsync(string tableName, DataTable dataTable, CancellationToken token)
        {
            var jarray = JArray.FromObject(dataTable);
            var rawsToPost = jarray
                .AsParallel()
                .Select(raw => new CloudSearchTableRaw() { Command = CloudSearchTableRawCommand.Delete, Fields = raw });

            return this.UploadTableRawsAsync(tableName, rawsToPost, token);
        }

        /// <summary>
        /// Deletes the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="dataTable">The data table.</param>
        public void DeleteTableRaws(string tableName, DataTable dataTable)
        {
            this.DeleteTableRawsAsync(tableName, dataTable, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Deletes the table raws by primary keys.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="primaryKeys">The primary keys.</param>
        /// <param name="primaryKeyColumnName">Name of the primary key column.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task DeleteTableRawsByPrimaryKeysAsync(string tableName, IEnumerable<string> primaryKeys, string primaryKeyColumnName, CancellationToken token)
        {
            var raws = primaryKeys
                .AsParallel()
                .Select(k => CloudSearchTableRaw.FromObject(new KeyValuePair<string, string>(primaryKeyColumnName, k), CloudSearchTableRawCommand.Delete));

            return this.UploadTableRawsAsync(tableName, raws, token);
        }

        /// <summary>
        /// Deletes the table raws by primary keys.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="primaryKeys">The primary keys.</param>
        /// <param name="primaryKeyColumnName">Name of the primary key column.</param>
        public void DeleteTableRawsByPrimaryKeys(string tableName, IEnumerable<string> primaryKeys, string primaryKeyColumnName)
        {
            this.DeleteTableRawsByPrimaryKeysAsync(tableName, primaryKeys, primaryKeyColumnName, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Uploads the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task UploadTableRawsAsync(string tableName, IEnumerable<CloudSearchTableRaw> raws, CancellationToken token)
        {
            return this.Account.CreateCloudSearchDocClient()
                .UploadCloudSearchTableRaws(this.Name, tableName, raws, token);
        }

        /// <summary>
        /// Uploads the table raws.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="raws">The raws.</param>
        public void UploadTableRaws(string tableName, IEnumerable<CloudSearchTableRaw> raws)
        {
            this.UploadTableRawsAsync(tableName, raws, CancellationToken.None).Wait();
        }

        /// <summary>
        /// Uploads the table rows by json.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="json">The json.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task UploadTableRowsByJsonAsync(string tableName, string json, CancellationToken token)
        {
            JArray jarray = JArray.Parse(json);
            return this.Account.CreateCloudSearchDocClient()
                .UploadJsonTokens(this.Name, tableName, jarray, token);
        }

        /// <summary>
        /// Uploads the table rows by json.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="json">The json.</param>
        public void UploadTableRowsByJson(string tableName, string json)
        {
            this.UploadTableRowsByJsonAsync(tableName, json, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Uploads the table rows from json file.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task.</returns>
        public Task UploadTableRowsFromJsonFileAsync(string tableName, string filePath, CancellationToken token)
        {
            return this.Account.CreateCloudSearchDocClient()
                .UploadFileAsync(this.Name, tableName, filePath, token);
        }

        /// <summary>
        /// Uploads the table rows from json file.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="filePath">The file path.</param>
        public void UploadTableRowsFromJsonFile(string tableName, string filePath)
        {
            this.UploadTableRowsFromJsonFileAsync(tableName, filePath, CancellationToken.None)
                .Wait();
        }

        /// <summary>
        /// Gets the suggest list asynchronous.
        /// </summary>
        /// <param name="queryString">The query string.</param>
        /// <param name="suggesionCount">The suggesion count.</param>
        /// <param name="suggestName">Name of the suggest.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;IEnumerable&lt;CloudSearchSuggestion&gt;&gt;.</returns>
        public Task<IEnumerable<CloudSearchSuggestion>> GetSuggestListAsync(string queryString, int suggesionCount, string suggestName, CancellationToken token)
        {
            return this.Account.CreateCloudSearchSuggestionClient()
                .GetSuggestionListAsync(this.Name, suggestName, suggesionCount, queryString, token);
        }

        /// <summary>
        /// Gets the suggest list.
        /// </summary>
        /// <param name="queryString">The query string.</param>
        /// <param name="suggesionCount">The suggesion count.</param>
        /// <param name="suggestName">Name of the suggest.</param>
        /// <returns>IEnumerable&lt;CloudSearchSuggestion&gt;.</returns>
        public IEnumerable<CloudSearchSuggestion> GetSuggestList(string queryString, int suggesionCount, string suggestName)
        {
            return this.GetSuggestListAsync(queryString, suggesionCount, suggestName, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Gets the error logs.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortMode">The sort mode.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchApplicationErrorLog&gt;.</returns>
        public Task<CloudSearchApplicationErrorLog> GetErrorLogsAsync(int page, int pageSize, CloudSearchSortMode sortMode, CancellationToken token)
        {
            return this.Account.CreateCloudSearchErrorClient()
                .GetErrorLogsAsync(this.Name, page, pageSize, sortMode, token);
        }

        /// <summary>
        /// Gets the error logs.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortMode">The sort mode.</param>
        /// <returns>CloudSearchApplicationErrorLog.</returns>
        public CloudSearchApplicationErrorLog GetErrorLogs(int page, int pageSize, CloudSearchSortMode sortMode)
        {
            return this.GetErrorLogsAsync(page, pageSize, sortMode, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Rebuilds the index asynchronous.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchTask&gt;.</returns>
        public Task<CloudSearchTask> RebuildIndexAsync(CancellationToken token)
        {
            return this.Account.CreateCloudSearchIndexClient()
                .RebuildIndexAsync(this.Name, null, token);
        }

        /// <summary>
        /// Rebuilds the index.
        /// </summary>
        /// <returns>CloudSearchTask.</returns>
        public CloudSearchTask RebuildIndex()
        {
            return this.RebuildIndexAsync(CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Rebuilds the index asynchronous.
        /// </summary>
        /// <param name="importTableName">Name of the import table.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchTask&gt;.</returns>
        public Task<CloudSearchTask> RebuildIndexAsync(string importTableName, CancellationToken token)
        {
            return this.Account.CreateCloudSearchIndexClient()
                .RebuildIndexAsync(this.Name, importTableName, token);
        }

        /// <summary>
        /// Rebuilds the index.
        /// </summary>
        /// <param name="importTableName">Name of the import table.</param>
        /// <returns>CloudSearchTask.</returns>
        public CloudSearchTask RebuildIndex(string importTableName)
        {
            return this.RebuildIndexAsync(importTableName, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Searches the asynchronous.
        /// </summary>
        /// <param name="option">The option.</param>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchResult&gt;.</returns>
        public Task<CloudSearchResult> SearchAsync(
            CloudSearchOption option,
            SearchExpression queryExpression,
            CloudSearchConfiguration configuration,
            CancellationToken token)
        {
            CloudSearchExpressions expressions = new CloudSearchExpressions()
            {
                QueryExpression = queryExpression
            };

            return this.SearchAsync(option, expressions, configuration, token);
        }

        /// <summary>
        /// Searches the specified option.
        /// </summary>
        /// <param name="option">The option.</param>
        /// <param name="queryExpression">The query expression.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>CloudSearchResult.</returns>
        public CloudSearchResult Search(
            CloudSearchOption option,
            SearchExpression queryExpression,
            CloudSearchConfiguration configuration)
        {
            return this.SearchAsync(option, queryExpression, configuration, CancellationToken.None)
                .Result;
        }

        /// <summary>
        /// Searches the asynchronous.
        /// </summary>
        /// <param name="option">The option.</param>
        /// <param name="expressions">The expressions.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <returns>Task&lt;CloudSearchResult&gt;.</returns>
        public Task<CloudSearchResult> SearchAsync(
            CloudSearchOption option,
            CloudSearchExpressions expressions,
            CloudSearchConfiguration configuration,
            CancellationToken token)
        {
            return this.Account.CreateCloudSearchSearchClient()
                .SearchAsync(
                    new string[] { this.Name },
                    option,
                    expressions.QueryExpression,
                    expressions.FilterExpression,
                    expressions.SortExpression,
                    expressions.AggregateExpression,
                    expressions.DistinctExpression,
                    expressions.KeyValuePairExpression,
                    configuration,
                    token);
        }

        /// <summary>
        /// Searches the specified option.
        /// </summary>
        /// <param name="option">The option.</param>
        /// <param name="expressions">The expressions.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>CloudSearchResult.</returns>
        public CloudSearchResult Search(
            CloudSearchOption option,
            CloudSearchExpressions expressions,
            CloudSearchConfiguration configuration)
        {
            return this.SearchAsync(option, expressions, configuration, CancellationToken.None)
                .Result;
        }
    }
}
