﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTableColumn.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class CloudSearchTableColumn.
    /// </summary>
    [DataContract]
    public class CloudSearchTableColumn
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "name")]
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the is multi.
        /// </summary>
        /// <value>The is multi.</value>
        [DataMember(Name = "is_multi")]
        public string IsMulti { get; set; }

        /// <summary>
        /// Gets or sets the is pk.
        /// </summary>
        /// <value>The is pk.</value>
        [DataMember(Name = "is_pk")]
        public string IsPK { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        [DataMember(Name = "src")]
        public string Src { get; set; }
    }
}
