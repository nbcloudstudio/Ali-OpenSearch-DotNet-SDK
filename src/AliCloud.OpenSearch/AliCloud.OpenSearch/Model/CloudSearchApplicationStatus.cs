﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchApplicationStatus.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;
    using AliCloud.OpenSearch.JsonConverters;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchApplicationStatus.
    /// </summary>
    [DataContract]
    public class CloudSearchApplicationStatus
    {
        /// <summary>
        /// Gets or sets the name of the index.
        /// </summary>
        /// <value>The name of the index.</value>
        [DataMember(Name = "index_name")]
        public string IndexName { get; set; }

        /// <summary>
        /// Gets or sets the pv.
        /// </summary>
        /// <value>The pv.</value>
        [DataMember(Name = "pv")]
        public int PV { get; set; }

        /// <summary>
        /// Gets or sets the document last update time.
        /// </summary>
        /// <value>The document last update time.</value>
        [DataMember(Name = "doc_last_update_time")]
        public int DocLastUpdateTime { get; set; }

        /// <summary>
        /// Gets or sets the total document number.
        /// </summary>
        /// <value>The total document number.</value>
        [DataMember(Name = "total_doc_num")]
        public int TotalDocNum { get; set; }

        /// <summary>
        /// Gets or sets the quota.
        /// </summary>
        /// <value>The quota.</value>
        [DataMember(Name = "quota")]
        public CloudSearchQuota Quota { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>The fields.</value>
        [DataMember(Name = "fields")]
        public CloudSearchFields Fields { get; set; }

        /// <summary>
        /// Gets or sets the indexes.
        /// </summary>
        /// <value>The indexes.</value>
        [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
        [DataMember(Name = "indexes")]
        public CloudSearchItemCollection<CloudSearchTableIndex> Indexes { get; set; }
    }
}
