﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTableJoinMap.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using AliCloud.OpenSearch.JsonConverters;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchTableJoinMap.
    /// </summary>
    [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
    public class CloudSearchTableJoinMap : CloudSearchItemCollection<CloudSearchTableJoinItem>
    {
    }

    /// <summary>
    /// Class CloudSearchTableJoinItem.
    /// </summary>
    [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
    public class CloudSearchTableJoinItem : CloudSearchItemCollection<CloudSearchTableJoinItemDefine[]>
    {
    }

    /// <summary>
    /// Class CloudSearchTableJoinItemDefine.
    /// </summary>
    [DataContract]
    public class CloudSearchTableJoinItemDefine
    {
        /// <summary>
        /// Gets or sets the table.
        /// </summary>
        /// <value>The table.</value>
        [DataMember(Name = "table")]
        public string Table { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        [DataMember(Name = "key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the join.
        /// </summary>
        /// <value>The join.</value>
        [DataMember(Name = "join")]
        public Dictionary<string, string> Join { get; set; }
    }
}
