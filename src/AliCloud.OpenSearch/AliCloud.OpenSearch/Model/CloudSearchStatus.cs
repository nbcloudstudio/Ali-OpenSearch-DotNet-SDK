﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchStatus.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    /// <summary>
    /// Enum CloudSearchStatus
    /// </summary>
    public enum CloudSearchStatus
    {
        /// <summary>
        /// Unknow status
        /// </summary>
        Unkown,// some api not return status, eg: suggest

        /// <summary>
        /// The request return OK
        /// </summary>
        OK,

        /// <summary>
        /// The request failed
        /// </summary>
        FAIL
    }
}
