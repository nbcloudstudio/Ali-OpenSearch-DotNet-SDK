﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchError.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;
    /// <summary>
    /// Class CloudSearchError.
    /// </summary>
    [DataContract]
    public class CloudSearchError
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        [DataMember(Name = "code")]
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
