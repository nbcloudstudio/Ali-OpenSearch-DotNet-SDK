﻿using System.Collections.Generic;
// ----------------------------------------------------------------------
// <copyright file="CloudSearchTable.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using AliCloud.OpenSearch.JsonConverters;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchTable.
    /// </summary>
    [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
    public class CloudSearchTable : CloudSearchItemCollection<CloudSearchTableColumn>
    {
        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>The columns.</value>
        public Dictionary<string, CloudSearchTableColumn> Columns
        {
            get
            {
                return this.items;
            }
        }
    }
}
