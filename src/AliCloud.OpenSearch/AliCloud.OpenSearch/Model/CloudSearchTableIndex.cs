﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTableIndex.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class CloudSearchTableIndex.
    /// </summary>
    [DataContract]
    public class CloudSearchTableIndex
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the is filter.
        /// </summary>
        /// <value>The is filter.</value>
        [DataMember(Name = "is_filter")]
        public string IsFilter { get; set; }

        /// <summary>
        /// Gets or sets the is search.
        /// </summary>
        /// <value>The is search.</value>
        [DataMember(Name = "is_search")]
        public string IsSearch { get; set; }

        /// <summary>
        /// Gets or sets the index to.
        /// </summary>
        /// <value>The index to.</value>
        [DataMember(Name = "index_to")]
        public string[] IndexTo { get; set; }

        /// <summary>
        /// Gets or sets the is display.
        /// </summary>
        /// <value>The is display.</value>
        [DataMember(Name = "is_display")]
        public string IsDisplay { get; set; }

        /// <summary>
        /// Gets or sets the is aggregate.
        /// </summary>
        /// <value>The is aggregate.</value>
        [DataMember(Name = "is_aggregate")]
        public string IsAggregate { get; set; }
    }
}
