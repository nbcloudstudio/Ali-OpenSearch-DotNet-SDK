﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSortMode.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    /// <summary>
    /// Enum CloudSearchSortMode
    /// </summary>
    public enum CloudSearchSortMode
    {
        /// <summary>
        /// The asc
        /// </summary>
        ASC,
        /// <summary>
        /// The desc
        /// </summary>
        DESC
    }
}
