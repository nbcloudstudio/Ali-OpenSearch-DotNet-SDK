﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchQuota.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using AliCloud.OpenSearch.JsonConverters;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CloudSearchQuota.
    /// </summary>
    [DataContract]
    [JsonConverter(typeof(CloudSearchItemCollectionJsonConverter))]
    public class CloudSearchQuota : CloudSearchItemCollection<CloudSearchQuotaItem>
    {
    }

    /// <summary>
    /// Class CloudSearchQuotaItem.
    /// </summary>
    [DataContract]
    public class CloudSearchQuotaItem
    {
        /// <summary>
        /// Gets or sets a value indicating whether [out of limit].
        /// </summary>
        /// <value><c>true</c> if [out of limit]; otherwise, <c>false</c>.</value>
        [DataMember(Name = "out_of_limit")]
        public bool OutOfLimit { get; set; }
    }
}
