﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTableRawCommand.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    /// <summary>
    /// Enum CloudSearchTableRawCommand
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CloudSearchTableRawCommand
    {
        /// <summary>
        /// The add
        /// </summary>
        Add,
        /// <summary>
        /// The update
        /// </summary>
        Update,
        /// <summary>
        /// The delete
        /// </summary>
        Delete
    }
}
