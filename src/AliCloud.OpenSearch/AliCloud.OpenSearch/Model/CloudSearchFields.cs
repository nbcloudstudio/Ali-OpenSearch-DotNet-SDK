﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchFields.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class CloudSearchFields.
    /// </summary>
    [DataContract]
    public class CloudSearchFields
    {
        /// <summary>
        /// Gets or sets from table.
        /// </summary>
        /// <value>From table.</value>
        [DataMember(Name = "from_table")]
        public CloudSearchFieldsFromTable FromTable { get; set; }

        /// <summary>
        /// Gets or sets the merge table.
        /// </summary>
        /// <value>The merge table.</value>
        [DataMember(Name = "merge_table")]
        public CloudSearchTable MergeTable { get; set; }

        /// <summary>
        /// Gets or sets the primary key.
        /// </summary>
        /// <value>The primary key.</value>
        [DataMember(Name = "primary_key")]
        public string PrimaryKey { get; set; }
    }
}
