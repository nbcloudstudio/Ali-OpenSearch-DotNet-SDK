﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchFilter.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliCloud.OpenSearch.Common;

namespace AliCloud.OpenSearch.Model.Search
{
    /// <summary>
    /// Class FilterCondition.
    /// </summary>
    public class FilterExpression : SearchExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterExpression"/> class.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="comparison">The comparison.</param>
        /// <param name="value">The value.</param>
        internal FilterExpression(string field, FilterComparison comparison, object value)
        {
            this.Field = field;
            this.Value = value;
            this.Comparison = comparison;
        }

        /// <summary>
        /// Gets the field.
        /// </summary>
        /// <value>The field.</value>
        public string Field { get; private set; }

        /// <summary>
        /// Gets the comparison.
        /// </summary>
        /// <value>The comparison.</value>
        public FilterComparison Comparison { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value { get; private set; }

        /// <summary>
        /// Gets the condition.
        /// </summary>
        /// <value>The condition.</value>
        public override string Expression
        {
            get
            {
                // todo: check the value type here.
                return "{0}{1}{2}".InvariantFormat(this.Field, this.Comparison, this.Value);
            }
        }
    }

    /// <summary>
    /// Class FilterCombineCondition.
    /// </summary>
    public class FilterCombineExpression : FilterExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterCombineExpression"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="logicOperator">The comparison.</param>
        /// <param name="right">The right.</param>
        internal FilterCombineExpression(FilterExpression left, FilterLogicOperator logicOperator, FilterExpression right)
            : base(null, null, null)
        {
            this.Left = left;
            this.Right = right;
            this.LogicOperator = logicOperator;
        }

        /// <summary>
        /// Gets the left.
        /// </summary>
        /// <value>The left.</value>
        public FilterExpression Left { get; private set; }

        /// <summary>
        /// Gets the right.
        /// </summary>
        /// <value>The right.</value>
        public FilterExpression Right { get; private set; }

        public FilterLogicOperator LogicOperator { get; private set; }

        /// <summary>
        /// Gets the condition.
        /// </summary>
        /// <value>The condition.</value>
        public override string Expression
        {
            get { return "({0}) {1} ({2})".InvariantFormat(this.Left, this.LogicOperator, this.Right); }
        }
    }

    /// <summary>
    /// Class FilterComparison.
    /// </summary>
    public class FilterComparison : CloudSearchSymbol
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterComparison"/> class.
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        private FilterComparison(string symbol)
            : base(symbol)
        {
        }

        /// <summary>
        /// The equals to
        /// </summary>
        public static FilterComparison EqualsTo = new FilterComparison("=");
        /// <summary>
        /// The greater than
        /// </summary>
        public static FilterComparison GreaterThan = new FilterComparison(">");
        /// <summary>
        /// The less than
        /// </summary>
        public static FilterComparison LessThan = new FilterComparison("<");
        /// <summary>
        /// The not greater than
        /// </summary>
        public static FilterComparison NotGreaterThan = new FilterComparison("<=");
        /// <summary>
        /// The not less than
        /// </summary>
        public static FilterComparison NotLessThan = new FilterComparison(">=");
        /// <summary>
        /// The not equals to
        /// </summary>
        public static FilterComparison NotEqualsTo = new FilterComparison("!=");
    }

    /// <summary>
    /// Class FilterLogicOperator.
    /// </summary>
    public class FilterLogicOperator : CloudSearchSymbol
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterLogicOperator"/> class.
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        private FilterLogicOperator(string symbol)
            : base(symbol)
        {
        }

        /// <summary>
        /// The and
        /// </summary>
        public static FilterLogicOperator And = new FilterLogicOperator("AND");
        /// <summary>
        /// The or
        /// </summary>
        public static FilterLogicOperator Or = new FilterLogicOperator("OR");
    }
}
