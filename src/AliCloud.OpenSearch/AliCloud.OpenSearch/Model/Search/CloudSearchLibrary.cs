﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchLibrary.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Converters;

    /// <summary>
    /// Class CloudSearchLibrary.
    /// </summary>
    public class CloudSearchLibrary
    {
        /// <summary>
        /// Generates the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public static string GenerateValue(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var type = value.GetType();

            if (type.IsSubclassOf(typeof(ValueType)))
            {
                return Convert.ToString(value);
            }
            else
            {
                // todo: check more datatype
                return "'{0}'".InvariantFormat(value);
            }
        }
    }

    /// <summary>
    /// Class CloudSearchSymbol.
    /// </summary>
    public class CloudSearchSymbol
    {
        /// <summary>
        /// The symbol
        /// </summary>
        private readonly string symbol;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchSymbol"/> class.
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        protected CloudSearchSymbol(string symbol)
        {
            this.symbol = symbol;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.symbol;
        }
    }

    /// <summary>
    /// Class SearchExpression.
    /// </summary>
    public class SearchExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchExpression"/> class.
        /// </summary>
        public SearchExpression()
        {
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public virtual string Expression
        {
            get
            {
                string connector = ";";
                var connectorAttribute = this.GetType().GetCustomAttribute<CloudSearchConnectorAttribute>();
                if (connectorAttribute != null)
                {
                    connector = connectorAttribute.Connector;
                }
                var items = this.GetType()
                    .GetProperties()
                    .Where(p => p.GetCustomAttribute<CloudSearchPropertyAttribute>() != null)
                    .Select(p =>
                    {
                        var attr = p.GetCustomAttribute<CloudSearchPropertyAttribute>();
                        var name = attr.ExpressionName;
                        var value = attr.ConvertValue(p.GetValue(this));
                        if (string.IsNullOrEmpty(value))
                        {
                            return null;
                        }

                        return "{0}{1}{2}".InvariantFormat(name, attr.Symbol, value);
                    }).ToList().Where(p => !string.IsNullOrEmpty(p));

                return string.Join(connector, items);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.Expression;
        }


        /// <summary>
        /// Performs an implicit conversion from <see cref="System.String"/> to <see cref="SearchExpression"/>.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator SearchExpression(string expression)
        {
            return new SearchStringExpression(expression);
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="SearchExpression"/> to <see cref="String"/>.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns>The result of the conversion.</returns>
        public static explicit operator String(SearchExpression expression)
        {
            return Convert.ToString(expression);
        }
    }

    /// <summary>
    /// Class SearchStringExpression.
    /// </summary>
    public class SearchStringExpression : SearchExpression
    {
        /// <summary>
        /// The expression
        /// </summary>
        private string expression;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchStringExpression"/> class.
        /// </summary>
        /// <param name="expression">The expression.</param>
        public SearchStringExpression(string expression)
        {
            this.expression = expression;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression
        {
            get
            {
                return this.expression;
            }
        }
    }

    /// <summary>
    /// Class SearchCombineExpression.
    /// </summary>
    public abstract class SearchCombineExpression : SearchExpression
    {
        /// <summary>
        /// Gets the left.
        /// </summary>
        /// <value>The left.</value>
        public SearchExpression Left { get; private set; }

        /// <summary>
        /// Gets the right.
        /// </summary>
        /// <value>The right.</value>
        public SearchExpression Right { get; private set; }

        /// <summary>
        /// Gets the separator.
        /// </summary>
        /// <value>The separator.</value>
        public string Separator { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchCombineExpression"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="separator">The separator.</param>
        /// <param name="right">The right.</param>
        public SearchCombineExpression(SearchExpression left, string separator, SearchExpression right)
        {
            this.Left = left;
            this.Separator = separator;
            this.Right = right;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression
        {
            get { return "{0}{1}{2}".InvariantFormat(this.Left, this.Separator, this.Right); }
        }
    }

    /// <summary>
    /// Class CloudSearchPropertyAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CloudSearchPropertyAttribute : Attribute
    {
        /// <summary>
        /// The converters
        /// </summary>
        private static Lazy<ConcurrentDictionary<string, CloudSearchPropertyValueConverter>> converters = new Lazy<ConcurrentDictionary<string, CloudSearchPropertyValueConverter>>(
            () => new ConcurrentDictionary<string, CloudSearchPropertyValueConverter>(StringComparer.OrdinalIgnoreCase), LazyThreadSafetyMode.PublicationOnly);

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchPropertyAttribute"/> class.
        /// </summary>
        /// <param name="expressionName">Name of the expression.</param>
        /// <param name="symbol">The symbol.</param>
        public CloudSearchPropertyAttribute(string expressionName, string symbol)
        {
            this.ExpressionName = expressionName;
            this.Symbol = symbol;
            this.Nullable = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchPropertyAttribute"/> class.
        /// </summary>
        /// <param name="expressionName">Name of the expression.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="convertType">Type of the convert.</param>
        public CloudSearchPropertyAttribute(string expressionName, string symbol, Type convertType)
            : this(expressionName, symbol)
        {
            this.ConvertType = convertType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchPropertyAttribute"/> class.
        /// </summary>
        /// <param name="expressionName">Name of the expression.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="nullable">if set to <c>true</c> [nullable].</param>
        public CloudSearchPropertyAttribute(string expressionName, string symbol, bool nullable)
            : this(expressionName, symbol)
        {
            this.Nullable = nullable;
        }

        /// <summary>
        /// Gets the name of the expression.
        /// </summary>
        /// <value>The name of the expression.</value>
        public string ExpressionName { get; private set; }

        /// <summary>
        /// Gets the symbol.
        /// </summary>
        /// <value>The symbol.</value>
        public string Symbol { get; private set; }

        /// <summary>
        /// Gets the type of the convert.
        /// </summary>
        /// <value>The type of the convert.</value>
        public Type ConvertType { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CloudSearchPropertyAttribute"/> is nullable.
        /// </summary>
        /// <value><c>true</c> if nullable; otherwise, <c>false</c>.</value>
        public bool Nullable { get; set; }

        /// <summary>
        /// Converts the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>System.String.</returns>
        public string ConvertValue(object value)
        {
            if (!this.Nullable)
            {
                ValidationUtility.AssertNotNullOrEmpty(this.ExpressionName, value);
            }

            if (this.ConvertType != null)
            {
                return this.GetConverter().Convert(value);
            }

            return Convert.ToString(value);
        }

        /// <summary>
        /// Gets the converter.
        /// </summary>
        /// <returns>CloudSearchPropertyValueConverter.</returns>
        /// <exception cref="System.InvalidCastException">The converter type {0} is not a sub class of {1}.InvariantFormat(this.ConvertType.FullName, typeof(CloudSearchPropertyValueConverter).FullName)</exception>
        private CloudSearchPropertyValueConverter GetConverter()
        {
            CloudSearchPropertyValueConverter converter = null;
            if (this.ConvertType != null)
            {
                if (!this.ConvertType.IsSubclassOf(typeof(CloudSearchPropertyValueConverter)))
                {
                    throw new InvalidCastException("The converter type {0} is not a sub class of {1}".InvariantFormat(this.ConvertType.FullName, typeof(CloudSearchPropertyValueConverter).FullName));
                }

                if (!converters.Value.TryGetValue(this.ConvertType.FullName, out converter))
                {
                    converter = converters.Value
                         .GetOrAdd(
                             this.ConvertType.FullName,
                             Activator.CreateInstance(this.ConvertType) as CloudSearchPropertyValueConverter);
                }
            }

            return converter;
        }
    }

    /// <summary>
    /// Class CloudSearchConnectorAttribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class CloudSearchConnectorAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchConnectorAttribute"/> class.
        /// </summary>
        /// <param name="connector">The connector.</param>
        public CloudSearchConnectorAttribute(string connector)
        {
            this.Connector = connector;
        }

        /// <summary>
        /// Gets or sets the connector.
        /// </summary>
        /// <value>The connector.</value>
        public string Connector { get; set; }
    }
}
