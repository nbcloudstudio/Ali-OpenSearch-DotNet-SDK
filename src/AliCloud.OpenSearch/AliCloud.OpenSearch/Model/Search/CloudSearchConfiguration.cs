﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchConfiguration.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using AliCloud.OpenSearch.Common;
    using AliCloud.OpenSearch.Converters;

    /// <summary>
    /// Class CloudSearchConfiguration.
    /// </summary>
    [CloudSearchConnector(",")]
    public class CloudSearchConfiguration : SearchExpression
    {
        /// <summary>
        /// Gets or sets the start.
        /// </summary>
        /// <value>The start.</value>
        [CloudSearchProperty("start", ":")]
        public int Start { get; set; }

        /// <summary>
        /// Gets or sets the hit.
        /// </summary>
        /// <value>The hit.</value>
        [CloudSearchProperty("hit", ":")]
        public int Hit { get; set; }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        /// <value>The format.</value>
        [CloudSearchProperty("format", ":", typeof(CloudSearchPropertyValueLowerCaseConvertor))]
        public SearchFormat Format { get; set; }

        /// <summary>
        /// Gets or sets the size of the rerank.
        /// </summary>
        /// <value>The size of the rerank.</value>
        [CloudSearchProperty("rerank_size", ":")]
        public int RerankSize { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            ValidationUtility.AssertRange("start", this.Start, 0, 5000);
            ValidationUtility.AssertRange("hit", this.Hit, 0, 500);
            ValidationUtility.AssertRange("rerankSize", this.RerankSize, 0, 2000);

            // return "start:{0},hit:{1},format:{2},rerank_size:{3}".InvariantFormat(this.Start, this.Hit, Format.ToString().ToLower(), this.RerankSize);
            return base.ToString();
        }
    }

    /// <summary>
    /// Enum SearchFormat
    /// </summary>
    public enum SearchFormat
    {
        /// <summary>
        /// The json
        /// </summary>
        Json,

        /// <summary>
        /// The full json
        /// </summary>
        FullJson,

        /// <summary>
        /// The XML
        /// </summary>
        Xml
    }
}
