﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchExpressions.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliCloud.OpenSearch.Model.Search
{
    /// <summary>
    /// Class CloudSearchExpressions.
    /// </summary>
    public class CloudSearchExpressions
    {
        /// <summary>
        /// Gets or sets the query expression.
        /// </summary>
        /// <value>The query expression.</value>
        public SearchExpression QueryExpression { get; set; }
        /// <summary>
        /// Gets or sets the filter expression.
        /// </summary>
        /// <value>The filter expression.</value>
        public SearchExpression FilterExpression { get; set; }
        /// <summary>
        /// Gets or sets the sort expression.
        /// </summary>
        /// <value>The sort expression.</value>
        public SearchExpression SortExpression { get; set; }
        /// <summary>
        /// Gets or sets the aggregate expression.
        /// </summary>
        /// <value>The aggregate expression.</value>
        public SearchExpression AggregateExpression { get; set; }
        /// <summary>
        /// Gets or sets the distinct expression.
        /// </summary>
        /// <value>The distinct expression.</value>
        public SearchExpression DistinctExpression { get; set; }
        /// <summary>
        /// Gets or sets the key value pair expression.
        /// </summary>
        /// <value>The key value pair expression.</value>
        public SearchExpression KeyValuePairExpression { get; set; }
    }
}
