﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchOption.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using System.Collections.Generic;

    /// <summary>
    /// Class CloudSearchOption.
    /// </summary>
    public class CloudSearchOption
    {
        /// <summary>
        /// Gets or sets the fetch fields.
        /// </summary>
        /// <value>The fetch fields.</value>
        public List<string> FetchFields { get; set; }

        /// <summary>
        /// Gets or sets the qp.
        /// </summary>
        /// <value>The qp.</value>
        public List<string> QP { get; set; }

        /// <summary>
        /// Gets or sets the disable.
        /// </summary>
        /// <value>The disable.</value>
        public string Disable { get; set; }

        /// <summary>
        /// Gets or sets the first name of the formula.
        /// </summary>
        /// <value>The first name of the formula.</value>
        public string FirstFormulaName { get; set; }

        /// <summary>
        /// Gets or sets the name of the formula.
        /// </summary>
        /// <value>The name of the formula.</value>
        public string FormulaName { get; set; }

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        /// <value>The summary.</value>
        public CloudSearchSummary Summary { get; set; }
    }

    /// <summary>
    /// Class CloudSearchSummary.
    /// </summary>
    [CloudSearchConnector(",")]
    public class CloudSearchSummary : SearchExpression
    {
        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        /// <value>The field.</value>
        [CloudSearchProperty("summary_field", ":")]
        public string Field { get; set; }

        /// <summary>
        /// Gets or sets the element.
        /// </summary>
        /// <value>The element.</value>
        [CloudSearchProperty("summary_element", ":")]
        public string Element { get; set; }

        /// <summary>
        /// Gets or sets the ellipsis.
        /// </summary>
        /// <value>The ellipsis.</value>
        [CloudSearchProperty("summary_ellipsis", ":")]
        public string Ellipsis { get; set; }

        /// <summary>
        /// Gets or sets the snapped.
        /// </summary>
        /// <value>The snapped.</value>
        [CloudSearchProperty("summary_snipped", ":")]
        public string Snapped { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>The length.</value>
        [CloudSearchProperty("summary_len", ":")]
        public string Len { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>The prefix.</value>
        [CloudSearchProperty("summary_prefix", ":")]
        public string Prefix { get; set; }

        /// <summary>
        /// Gets or sets the postfix.
        /// </summary>
        /// <value>The postfix.</value>
        [CloudSearchProperty("summary_postfix", ":")]
        public string Postfix { get; set; }
    }
}
