﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchDistinct.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using AliCloud.OpenSearch.Converters;

    [CloudSearchConnector(",")]
    public class DistinctExpression : SearchExpression
    {
        /// <summary>
        /// Gets or sets the dist key.
        /// </summary>
        /// <value>The dist key.</value>
        [CloudSearchProperty("dist_key", ":", false)]
        public string DistKey { get; set; }

        /// <summary>
        /// Gets or sets the dist times.
        /// </summary>
        /// <value>The dist times.</value>
        [CloudSearchProperty("dist_times", ":")]
        public int? DistTimes { get; set; }

        /// <summary>
        /// Gets or sets the dist count.
        /// </summary>
        /// <value>The dist count.</value>
        [CloudSearchProperty("dist_count", ":")]
        public int? DistCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DistinctExpression"/> is reserverd.
        /// </summary>
        /// <value><c>null</c> if [reserverd] contains no value, <c>true</c> if [reserverd]; otherwise, <c>false</c>.</value>
        [CloudSearchProperty("reserved", ":", typeof(CloudSearchPropertyValueLowerCaseConvertor))]
        public bool? Reserverd { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [update total hit].
        /// </summary>
        /// <value><c>null</c> if [update total hit] contains no value, <c>true</c> if [update total hit]; otherwise, <c>false</c>.</value>
        [CloudSearchProperty("update_total_hit", ":", typeof(CloudSearchPropertyValueLowerCaseConvertor))]
        public bool? UpdateTotalHit { get; set; }

        /// <summary>
        /// Gets or sets the dist filter.
        /// </summary>
        /// <value>The dist filter.</value>
        [CloudSearchProperty("dist_filter", ":")]
        public string DistFilter { get; set; }

        /// <summary>
        /// Gets or sets the grade.
        /// </summary>
        /// <value>The grade.</value>
        [CloudSearchProperty("grade", ":")]
        public float? Grade { get; set; }
    }
}
