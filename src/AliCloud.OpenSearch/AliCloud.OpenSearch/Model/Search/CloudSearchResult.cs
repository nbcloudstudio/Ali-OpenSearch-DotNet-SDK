﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchResult.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using System.Runtime.Serialization;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class CloudSearchResult.
    /// </summary>
    [DataContract]
    public class CloudSearchResult
    {
        /// <summary>
        /// Gets or sets the search time.
        /// </summary>
        /// <value>The search time.</value>
        [DataMember(Name = "searchtime")]
        public int SearchTime { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        [DataMember(Name = "total")]
        public int Total { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        [DataMember(Name = "num")]
        public int Num { get; set; }

        /// <summary>
        /// Gets or sets the view total.
        /// </summary>
        /// <value>The view total.</value>
        [DataMember(Name = "viewtotal")]
        public int ViewTotal { get; set; }

        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "items")]
        public JArray Items { get; set; }

        /// <summary>
        /// Gets or sets the facet.
        /// </summary>
        /// <value>The facet.</value>
        [DataMember(Name = "facet")]
        public JToken Facet { get; set; }
    }
}
