﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchAggregate.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    /// <summary>
    /// Class AggregateExpression.
    /// </summary>
    [CloudSearchConnector(",")]
    public class AggregateExpression : SearchExpression
    {
        /// <summary>
        /// Gets or sets the group key.
        /// </summary>
        /// <value>The group key.</value>
        [CloudSearchProperty("group_key", ":", false)]
        public string GroupKey { get; set; }

        /// <summary>
        /// Gets or sets the range.
        /// </summary>
        /// <value>The range.</value>
        [CloudSearchProperty("range", ":")]
        public string Range { get; set; }

        /// <summary>
        /// Gets or sets the aggregate fun.
        /// </summary>
        /// <value>The aggregate fun.</value>
        [CloudSearchProperty("agg_fun", ":", false)]
        public string AggFun { get; set; }

        /// <summary>
        /// Gets or sets the aggregate filter.
        /// </summary>
        /// <value>The aggregate filter.</value>
        [CloudSearchProperty("agg_filter", ":")]
        public string AggFilter { get; set; }

        /// <summary>
        /// Gets or sets the aggregate sampler threshold.
        /// </summary>
        /// <value>The aggregate sampler threshold.</value>
        [CloudSearchProperty("agg_sampler_threshold", ":")]
        public string AggSamplerThreshold { get; set; }

        /// <summary>
        /// Gets or sets the aggregate sampler step.
        /// </summary>
        /// <value>The aggregate sampler step.</value>
        [CloudSearchProperty("agg_sampler_step", ":")]
        public string AggSamplerStep { get; set; }

        /// <summary>
        /// Gets or sets the maximum group.
        /// </summary>
        /// <value>The maximum group.</value>
        [CloudSearchProperty("max_group", ":")]
        public int? MaxGroup { get; set; }
    }
}
