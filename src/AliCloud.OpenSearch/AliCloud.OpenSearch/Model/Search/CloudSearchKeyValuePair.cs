﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchKeyValuePair.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using AliCloud.OpenSearch.Common;

    /// <summary>
    /// Class KeyValuePairExpression.
    /// </summary>
    public class KeyValuePairExpression : SearchExpression
    {
        /// <summary>
        /// The key
        /// </summary>
        private readonly string key;
        /// <summary>
        /// The value
        /// </summary>
        private readonly string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValuePairExpression"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public KeyValuePairExpression(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression
        {
            get { return "{0}:{1}".InvariantFormat(this.key, this.value); }
        }
    }

    /// <summary>
    /// Class KeyValuePairCombineExpression.
    /// </summary>
    public class KeyValuePairCombineExpression : SearchCombineExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyValuePairCombineExpression"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        public KeyValuePairCombineExpression(SearchExpression left, SearchExpression right)
            : base(left, ",", right)
        {

        }
    }

}
