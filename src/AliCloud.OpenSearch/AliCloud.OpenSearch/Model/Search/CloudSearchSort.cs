﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchSort.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using System.Diagnostics;
    using System.Linq;
    using AliCloud.OpenSearch.Common;

    /// <summary>
    /// Class CloudSearchSort.
    /// </summary>
    public class CloudSearchSort
    {
        /// <summary>
        /// The rank
        /// </summary>
        public const string Rank = "RANK";
        /// <summary>
        /// The asc rank
        /// </summary>
        public static readonly SortExpression AscRank = new SortExpression(SortType.Asc, Rank);
        /// <summary>
        /// The desc rank
        /// </summary>
        public static readonly SortExpression DescRank = new SortExpression(SortType.Desc, Rank);

        /// <summary>
        /// Generates the sort expression.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="sentence">The sentence.</param>
        /// <returns>SortExpression.</returns>
        public static SortExpression GenerateSortExpression(SortType type, string sentence)
        {
            return new SortExpression(type, sentence);
        }

        /// <summary>
        /// Generates the combine sort expression.
        /// </summary>
        /// <param name="expressions">The expressions.</param>
        /// <returns>SortExpression.</returns>
        public static SortExpression GenerateCombineSortExpression(params SortExpression[] expressions)
        {
            return new SortExpression(SortType.None,
                string.Join(";", expressions.Select(e => e.Expression).ToArray()));
        }
    }

    /// <summary>
    /// Class SortType.
    /// </summary>
    public class SortType : CloudSearchSymbol
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchSymbol" /> class.
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        private SortType(string symbol)
            : base(symbol)
        {
        }

        /// <summary>
        /// The asc
        /// </summary>
        public static SortType Asc = new SortType("+");
        /// <summary>
        /// The desc
        /// </summary>
        public static SortType Desc = new SortType("-");

        /// <summary>
        /// The none
        /// </summary>
        internal static SortType None = new SortType("");
    }

    /// <summary>
    /// Class SortExpression.
    /// </summary>
    public class SortExpression : SearchExpression
    {
        /// <summary>
        /// The expression
        /// </summary>
        private readonly string expression;

        /// <summary>
        /// Create a new sort expression by sort type and sentence
        /// The sentence can be any string value, but if you pass an invalid value
        /// and use it create a query request, the request will failed.
        /// You can pass a single field name, eg: new SortExpression(SortType.Asc,"name")
        /// the parse value is: +name
        /// You can also pass a sentence, eg: new SortExpression(SortType.Asc,"age-seniority"
        /// The parse value is: +(age-seniority)
        /// </summary>
        /// <param name="type">The sort type</param>
        /// <param name="sentence">The sort field or sentence</param>
        public SortExpression(SortType type, string sentence)
        {
            Debug.WriteLine("=={0}===", type);
            this.expression = "{0}{1}".InvariantFormat(type, sentence);
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression { get { return this.expression; } }
    }
}
