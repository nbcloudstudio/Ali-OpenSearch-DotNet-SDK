﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchQuery.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model.Search
{
    using AliCloud.OpenSearch.Common;

    /// <summary>
    /// Class QueryExpression.
    /// </summary>
    public class QueryExpression : SearchExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryExpression"/> class.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="boost">The boost.</param>
        public QueryExpression(string field, object value, int boost = int.MinValue)
        {
            this.Field = field;
            this.Value = value;
            this.Boost = boost;
        }

        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        /// <value>The field.</value>
        public string Field { get; set; }
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value { get; set; }
        /// <summary>
        /// Gets or sets the boost.
        /// </summary>
        /// <value>The boost.</value>
        public int Boost { get; set; }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression
        {
            get
            {
                if (this.Boost != int.MinValue)
                {
                    ValidationUtility.AssertRange("Boost", this.Boost, 0, 99);
                    return "{0}:{1}^{2}".InvariantFormat(this.Field, CloudSearchLibrary.GenerateValue(this.Value), this.Boost);
                }
                else
                {
                    return "{0}:{1}".InvariantFormat(this.Field, CloudSearchLibrary.GenerateValue(this.Value));
                }
            }
        }
    }

    /// <summary>
    /// Class QueryCombineExpression.
    /// </summary>
    public class QueryCombineExpression : SearchExpression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryCombineExpression"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="op">The op.</param>
        /// <param name="right">The right.</param>
        public QueryCombineExpression(SearchExpression left, QueryOperator op, SearchExpression right)
        {
            this.Left = left;
            this.Operator = op;
            this.Right = right;
        }

        /// <summary>
        /// Gets the left.
        /// </summary>
        /// <value>The left.</value>
        public SearchExpression Left { get; private set; }

        /// <summary>
        /// Gets the right.
        /// </summary>
        /// <value>The right.</value>
        public SearchExpression Right { get; private set; }

        /// <summary>
        /// Gets or sets the operator.
        /// </summary>
        /// <value>The operator.</value>
        public QueryOperator Operator { get; set; }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public override string Expression
        {
            get
            {
                return "({0}){1}({2})"
                    .InvariantFormat(
                          this.Left.Expression,
                          this.Operator,
                          this.Right.Expression);
            }
        }
    }

    /// <summary>
    /// Class QueryOperator.
    /// </summary>
    public class QueryOperator : CloudSearchSymbol
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloudSearchSymbol" /> class.
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        private QueryOperator(string symbol)
            : base(symbol)
        {
        }

        /// <summary>
        /// The and
        /// </summary>
        public static QueryOperator And = new QueryOperator("AND");
        /// <summary>
        /// The or
        /// </summary>
        public static QueryOperator Or = new QueryOperator("OR");
        /// <summary>
        /// The and not
        /// </summary>
        public static QueryOperator AndNot = new QueryOperator("ANDNOT");
        /// <summary>
        /// The rank
        /// </summary>
        public static QueryOperator Rank = new QueryOperator("RANK");
    }
}
