﻿// ----------------------------------------------------------------------
// <copyright file="CloudSearchTask.cs" company="Aliyun-inc">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// -----------------------------------------------------------------------

namespace AliCloud.OpenSearch.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Class CloudSearchTask.
    /// </summary>
    [DataContract]
    public class CloudSearchTask
    {
        /// <summary>
        /// Gets or sets the task identifier.
        /// </summary>
        /// <value>The task identifier.</value>
        [DataMember(Name = "task_id")]
        public string TaskId { get; set; }
    }
}
