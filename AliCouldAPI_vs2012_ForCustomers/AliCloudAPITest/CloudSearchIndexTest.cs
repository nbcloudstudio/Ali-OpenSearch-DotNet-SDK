﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using AliCloud.com.API;

namespace AliCloudAPITest
{
    
    
    /// <summary>
    ///这是 CloudSearchIndexTest 的测试类，旨在
    ///包含所有 CloudSearchIndexTest 单元测试
    ///</summary>
    [TestClass()]
    public class CloudSearchIndexTest:CloudSearchApiAliyunBase
    {
        

        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///createIndex 的测试
        ///</summary>
        [TestMethod()]
        public void testCreateIndex()
        {
            string indexName ="testindexfromoldsdk"; // TODO: 初始化为适当的值
            CloudsearchIndex target = new CloudsearchIndex(indexName, this.api); // TODO: 初始化为适当的值
            string template = "builtin_news"; // TODO: 初始化为适当的值
            string desc = string.Empty; // TODO: 初始化为适当的值
            string expected = string.Empty; // TODO: 初始化为适当的值
            string result;
            result = target.createByTemplate(template, desc, false);
            Console.WriteLine(result);
            Assert.IsTrue(result.IndexOf("action=create&template=news&template_type=0")>=0);
            

        }
    }
}
