﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AliCloud.com.API;
using System.Collections;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace AliCloudAPITest
{
    /// <summary>
    /// UnitTest1 的摘要说明
    /// </summary>
    [TestClass]
    public class CloudSearchApiTest
    {
        CloudsearchApi api = null;
        public CloudSearchApiTest()
        {
            //
            //TODO: 在此处添加构造函数逻辑
            //
            //const string client_id = "6100898261336341";
            //const string secret_id = "e2199298c92fe4c26a7268c69cf92853";

            const string client_id = "uTlPHKQwYjNZMKRE";
            const string secret_id = "wVGINBsq62PVMLSgbJBEu8xfH6VXmG";
            // api = new CloudsearchApi(client_id, secret_id,"http://opensearch.console.aliyun.com/",1);
            api = new CloudsearchApi(client_id, secret_id, "http://opensearch.aliyuncs.com/", 1, "HMAC-SHA1",
              "1.0", 3000, true);

        }

        private int rep;
        private String RandomStr(int codeCount)
        {
            string str = string.Empty;
            long num2 = DateTime.Now.Ticks + this.rep;
            this.rep++;
            Random random = new Random(((int)(((ulong)num2) & 0xffffffffL)) | ((int)(num2 >> this.rep)));
            for (int i = 0; i < codeCount; i++)
            {
                char ch;
                int num = random.Next();
                if ((num % 2) == 0)
                {
                    ch = (char)(0x30 + ((ushort)(num % 10)));
                }
                else
                {
                    ch = (char)(0x41 + ((ushort)(num % 0x1a)));
                }
                str = str + ch.ToString();
            }
            return str;
        }


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性:
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestTopQuery()
        {
            CloudsearchAnalysis css = new CloudsearchAnalysis("hotel", this.api);
            String result = css.getTopQuery(100, 100);
            JObject jo = JObject.Parse(result);

            Console.WriteLine(result);
            Assert.AreEqual("OK", jo["status"]);
        }

        
        [TestMethod]
        public void TestPushHADoc()
        {
            CloudsearchDoc doc = new CloudsearchDoc("hotel_new", this.api);
            doc.pushHADocFile("d://hotel.data", "main");
        }


        [TestMethod]
        public void TestUnixTimeStamp()
        {
            long stamp = Utilities.getUnixTimeStamp();
            Console.WriteLine(stamp);
        }
        [TestMethod]
        public void TestCreateIndex()
        {
            String indexName = this.RandomStr(5);
            CloudsearchIndex target = new CloudsearchIndex(indexName, this.api); // TODO: 初始化为适当的值
            string template = "builtin_news"; // TODO: 初始化为适当的值
            string desc = string.Empty; // TODO: 初始化为适当的值
            bool templatType = true; // TODO: 初始化为适当的值
            string expected = string.Empty; // TODO: 初始化为适当的值
            string actual;
            actual = target.createByTemplate(template, desc, templatType);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            Console.WriteLine(actual);

            JObject jo = JObject.Parse(actual);

            //Console.WriteLine(result);
            Assert.AreEqual("OK", jo["status"]);

            
            actual = target.delete();
            Console.WriteLine(actual);


            //"result":{"index_name":"4N0F6H","description":""},"status":"OK"}
            jo = JObject.Parse(actual);
            Assert.AreEqual("OK", jo["status"]);
            Assert.AreEqual(1, jo["result"]);
        }

        //[TestMethod]
        //public void TestRenameIndex()
        //{

        //    CloudSearchIndex target = new CloudSearchIndex("testIndex11", this.api); // TODO: 初始化为适当的值

        //    String result = target.rename("xxx");
        //    Console.WriteLine(result);
        //    //Assert.AreEqual(expected, actual);
        //    //Assert.Inconclusive("验证此测试方法的正确性。");

        //}

        //[TestMethod]
        //public void TestDeleteIndex()
        //{

        //    CloudSearchIndex target = new CloudSearchIndex("xxx", this.api); // TODO: 初始化为适当的值

        //    String result = target.delete();
        //    Console.WriteLine(result);
        //    //Assert.AreEqual(expected, actual);
        //    //Assert.Inconclusive("验证此测试方法的正确性。");

        //}

        [TestMethod]
        public void TestStatusIndex()
        {

            CloudsearchIndex target = new CloudsearchIndex("ceshi", this.api); // TODO: 初始化为适当的值

            String result = target.status();
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
        }


        [TestMethod]
        public void TestListIndex()
        {

            CloudsearchIndex target = new CloudsearchIndex("ceshi", this.api); // TODO: 初始化为适当的值

            String result = target.listIndexes();
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);

            result = target.listIndexes(2,2);
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
        }


        [TestMethod]
        public void TestGetIndexName()
        {

            CloudsearchIndex target = new CloudsearchIndex("ceshi", this.api); // TODO: 初始化为适当的值

            String result = target.getIndexName();
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            Assert.AreEqual("ceshi", result);
        }

        [TestMethod]
        public void TestDocDetail()
        {

            CloudsearchDoc target = new CloudsearchDoc("bocheng_test", this.api); // TODO: 初始化为适当的值

            String result = target.detail("id","1");
            Console.WriteLine(result);
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
            Assert.AreEqual("1", jo["result"]["a"]);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");

        }


        [TestMethod]
        public void TestDocAdd()
        {

            CloudsearchDoc target = new CloudsearchDoc("ccc", this.api); // TODO: 初始化为适当的值
            String pk = this.RandomStr(5);
            String data = "[{\"fields\":{\"xxx\":\""+pk+"\"},\"cmd\":\"ADD\"}]";
            String result = target.add(data, "main");
            Console.WriteLine(result);
            Console.WriteLine(pk);
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");

        }

        [TestMethod]
        public void TestDocAdd1()
        {

            CloudsearchDoc target = new CloudsearchDoc("bocheng_test", this.api); // TODO: 初始化为适当的值

            String data = "[{\"fields\":{\"a\":\"1\"},\"cmd\":\"ADD\"}]";
            String result = target.add(data, "main");
            Console.WriteLine(result);
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");

        }


        [TestMethod]
        public void TestDocDel()
        {

            CloudsearchDoc target = new CloudsearchDoc("ccc", this.api); // TODO: 初始化为适当的值

            String data = "[{\"fields\":{\"xxx\":\"26N44\"},\"cmd\":\"DELETE\"}]";
            String result = target.add(data, "main");
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
        }

        [TestMethod]
        public void TestDocUpdate()
        {

            CloudsearchDoc target = new CloudsearchDoc("bocheng_test", this.api); // TODO: 初始化为适当的值

            String data = "[{\"fields\":{\"a\":\"1\",\"b\":\"test\"},\"cmd\":\"UPDATE\"}]";
            String result = target.add(data, "main");
            Console.WriteLine(result);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("验证此测试方法的正确性。");
            JObject jo = JObject.Parse(result);
            Assert.AreEqual("OK", jo["status"]);
        }

        [TestMethod]
        public void TestSearch()
        {

            Int32 page = 1;
            Int32 page_size = 20;
            String format = "json";
            String q = "suv";
            

            CloudsearchSearch search = new CloudsearchSearch(this.api);

            search.setFormat(format);
            search.setHits(page_size);
            search.setStartHit((page - 1) * page_size);
            
            //// sort
            //search.addSort("d", "-");
            //search.addSort("c", "-");
            //Console.WriteLine(search.getSortString());
            
            //// formula and formula name
            ////search.setFormulaName("test");
            ////search.addDistinct("is_sc", 3, 2);
            ////search.setFormulaName("default");
            ////search.setFormulaName("ran");
            ////search.setFormulaContent("random()");
            
            //// filter
            ////search.addFilter("c=0");
            //search.addFilter("d=72");
            
            //// query
            search.setQueryString("default:'" + q + "'");
            ////search.setQueryString("d:'0'");
            
            //// smart summary
            //search.addSummary("b", 30, "", "...", 1, "<em>", "</em>");
            //search.addSummary("e", 38, "em", "...", 2);
            
            //// aggregate
            //search.addAggregate("c", "count()");
            //search.addAggregate("c", "max(c)");
            ////search.addAggregate("date", "count()");
            
            //// distinct
            //search.addDistinct("d", 1, 2);
            ////search.addDistinct("date", array("dist_count" => 1, "dist_times" => 2, "grade" => 3, "reserved" => "true"));
            //search.addDistinct("none_dist");
            
            //// fetch fields
            ////search.addFetchFields(array("title", "date", "summary", "id", "url"));
            //search.addFetchFields("d");
            //search.addFetchFields("b");
            //search.addFetchFields("c");
            ////search.addFetchFields("title");

            search.addIndex("hotel");
            Dictionary<String, Object> opts = new Dictionary<String, Object>();
            String result = search.search(opts);
            Debug.WriteLine("======RESULT======");
            Debug.WriteLine(result);
            Debug.WriteLine("=========Agg========");
            Debug.WriteLine(search.getAggregateString());
        }

        [TestMethod]
        public void TestAPI()
        {
            var customD2 = new Dictionary<string, object>()
            {
                { "param1", new Dictionary<string, object>() { { "a", 1 }, {"b", 2 } } },
                { "param2", new object[] { "c", "d", new Dictionary<string, object>() { { "e", 1} } } }
            };


            String res = HttpBuildQueryHelper.FormatDictionary(customD2);
            Console.WriteLine(res);
        }

        /*[TestMethod]
        public void TestSearch()
        {

            Int32 page = 1;
            Int32 page_size = 20;
            String format = "json";
            String q = "广大";


            CloudSearchSearch search = new CloudSearchSearch(this.api);

            search.setFormat(format);
            search.setHits(page_size);
            search.setStartHit((page - 1) * page_size);

            // sort
            search.addSort("d", "+");
            search.addSort("c", "+");

            // formula and formula name
            //search.setFormulaName("test");
            //search.addDistinct("is_sc", 3, 2);
            //search.setFormulaName("default");
            search.setFormulaName("ran");
            search.setFormulaContent("random()");

            // filter
            //search.addFilter("is_sc=0");
            //search.addFilter("date='1'");

            // query
            search.setQueryString("default:'" + q + "'");

            // smart summary
            //search.addSummary("title", 30, "", "...", 1, "<em>", "</em>");
            //search.addSummary("summary", 110, "em", "...", 2);

            // aggregate
            //search.addAggregate("date", "count()");
            //search.addAggregate("date", "max(date)");
            //search.addAggregate("date", "count()");

            // distinct
            //search.addDistinct("is_sc", 1, 2);
            //search.addDistinct("date", array("dist_count" => 1, "dist_times" => 2, "grade" => 3, "reserved" => "true"));
            //search.addDistinct("none_dist");

            // fetch fields
            //search.addFetchFields(array("title", "date", "summary", "id", "url"));
            //search.addFetchFields("url");
            //search.addFetchFields("title");

            search.addIndex("bocheng_test");
            Dictionary<String, Object> opts = new Dictionary<String, Object>();
            String result = search.search(opts);
            Console.WriteLine(result);
            Console.WriteLine(search.getAggregateString());
        }*/
    }
}
