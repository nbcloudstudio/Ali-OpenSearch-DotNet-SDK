﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AliCloud.com.API;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Collections.Specialized;

namespace AliCloudAPITest
{
    /// <summary>
    /// UnitTest1 的摘要说明
    /// </summary>
    [TestClass]
    public class CloudSearchSearchTest : CloudSearchApiAliyunBase
    {

        //多个索引查询
        [TestMethod]
        public void TestMutiIndex()
        {
            String indexName1 = "index1";
            String indexName2 = "index2";

            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName1);
            search.addIndex(indexName2);
            search.setFormat("json");

            search.setQueryString("水杯");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("start:0");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:json");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:20");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=水杯");
            Assert.IsTrue(i >= 0);

            var index_name = queryDict["index_name"];
            Console.WriteLine(index_name);
            Assert.AreEqual("index1;index2", index_name);


        }

        //设置 formula_name参数 
        [TestMethod]
        public void TestSetFormulaName()
        {
            var search = new CloudsearchSearch(this.api);
            String indexName = "index1";
            search.addIndex(indexName);
            search.setFormat("json");

            search.setFormulaName("test1");

            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("start:0");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:json");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:20");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=");
            Assert.IsTrue(i >= 0);

            var formula_name = queryDict["formula_name"];
            Console.WriteLine(formula_name);
            Assert.AreEqual("test1", formula_name);


            Assert.AreEqual("test1", search.getFormulaName());
        }


        // 设置正确的飘红字段
        [TestMethod]
        public void testSearchSummary()
        {
            String indexName = "index1";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addSummary("security_key", 30, "em", "...", 1);
            search.setQueryString("搜索");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("start:0");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:json");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:20");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=搜索");
            Assert.IsTrue(i >= 0);

            var summary = queryDict["summary"];
            Console.WriteLine(summary);
            Assert.AreEqual("summary_field:security_key,summary_len:30,summary_element:em,summary_ellipsis:...,summary_snippet:1;", summary);


        }


        //单字段搜索
        [TestMethod]
        public void testQueryCq()
        {
            var indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            var q = "苏宁";
            search.setQueryString("security_key:" + q);
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("start:0");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:json");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:20");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=security_key:" + q);
            Assert.IsTrue(i >= 0);

        }


        // 按照单字段减序
        [TestMethod]
        public void testDeduceSort()
        {
            var search = new CloudsearchSearch(this.api);
            string indexName = "index";
            search.addIndex(indexName);
            search.setFormat("json");

            search.addSort("price", "-");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("sort=-price");
            Assert.IsTrue(i >= 0);


            search.removeSort("price");
        }



        //filter AND
        [TestMethod]
        public void testFilterAnd()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addFilter("price>12");
            search.addFilter("id<88");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("filter=price>12 AND id<88");
            Assert.IsTrue(i >= 0);


        }

        [TestMethod]
        public void testAggregateWithTwoField()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addAggregate("price", "count()");
            search.addAggregate("id", "count()");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("aggregate=group_key:price,agg_fun:count();group_key:id,agg_fun:count()");
            Assert.IsTrue(i >= 0);


            search.removeAggregate("price");
            search.removeAggregate("id");
        }

        [TestMethod]
        public void testFetchFields()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addFetchFields("id");
            search.addFetchFields("gmt_modified");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["fetch_fields"];

            Console.WriteLine(query);

            int i = query.IndexOf("id;gmt_modified");
            Assert.IsTrue(i >= 0);


        }

        //add all params 8
        [TestMethod]
        public void testDistinctWithAllParams()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addDistinct("user_id", 1, 2, "false", "price>12", "true", 25, "3.0");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("distinct=dist_key:user_id,dist_count:1,dist_times:2,reserved:false,dist_filter:price>12,update_total_count:true,max_item_count:25,grade:3");
            Assert.IsTrue(i >= 0);


        }

        [TestMethod]
        public void testPair()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");
            search.setPair("test1:test2");
            String result = search.search();
            Uri uri = new Uri(result);
            var queryDict = HttpUtility.ParseQueryString(uri.Query);

            String query = queryDict["query"];

            Console.WriteLine(query);

            int i = query.IndexOf("kvpairs=test1:test2");
            Assert.IsTrue(i >= 0);

            // this.assertFalse(strpos(result, "%26%26kvpairs%3Dtest1%3Atest2") === false);
        }

        [TestMethod]
        public void testCustomParams()
        {
            String indexName = "index";
            var search = new CloudsearchSearch(this.api);
            search.addIndex(indexName);
            search.setFormat("json");

            search.addCustomParam("test1", "test2");
            search.addCustomParam("test3", "test4");
            search.addCustomParam("test8", new string[]{"xxx","bbb"});
            String result = search.search();
            Console.WriteLine(result);
            Assert.IsTrue(result.IndexOf("test1=test2")>=0);
            Assert.IsTrue(result.IndexOf("test3=test4") >= 0);
            Assert.IsTrue(result.IndexOf("test8[0]=xxx") >= 0);
            Assert.IsTrue(result.IndexOf("test8[1]=bbb") >= 0);
        }


        /// <summary>
        /// 
        /// 测试搜索相关url
        /// 
        /// </summary>

        [TestMethod]
        public void TestAllSearch()
        {

            Int32 page = 1;
            Int32 page_size = 20;
            String format = "json";
            String q = "中文";


            CloudsearchSearch search = new CloudsearchSearch(this.api);

            search.setFormat(format);
            search.setHits(page_size);
            search.setStartHit((page - 1) * page_size);

            //// query
            search.setQueryString("default:'" + q + "'");


            search.addIndex("hotel");
            Dictionary<String, Object> opts = new Dictionary<String, Object>();
            String result = search.search(opts);
            Console.WriteLine(result);


            /*
             http://opensearch.console.aliyun.com/v2/api/search?
             * query=config=format:json,start:0,hit:20,rerank_size:200&&query=default:'中文'&index_name=hotel&Version=v2
             * &AccessKeyId=uTlPHKQwYjNZMKRE&SignatureMethod=HMAC-SHA1&SignatureVersion=1.0&SignatureNonce=
             * 14129937619908342&Timestamp=2014-10-11T02:16:01Z&Signature=3pxDIMoVHKI6YRKnD6FR34L9qPM=
             */

            Uri uri = new Uri(result);
            var n = HttpUtility.ParseQueryString(uri.Query);
            var query = n["query"];

            int i = query.IndexOf("start:0");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:json");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:20");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=default:'中文'");
            Assert.IsTrue(i >= 0);

            var index_name = n["index_name"];
            Assert.AreEqual("hotel", index_name);



            page = 2;
            page_size = 50;
            format = "xml";
            search.setFormat(format);
            search.setHits(page_size);
            search.setStartHit((page - 1) * page_size);

            search.removeIndex("hotel");
            search.addIndex("hotel2");

            opts = new Dictionary<String, Object>();
            result = search.search(opts);
            Console.WriteLine(result);

            uri = new Uri(result);
            n = HttpUtility.ParseQueryString(uri.Query);
            query = n["query"];

            i = query.IndexOf("start:50");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("format:xml");
            Assert.IsTrue(i >= 0);

            i = query.IndexOf("hit:50");
            Assert.IsTrue(i >= 0);
            i = query.IndexOf("query=default:'中文'");
            Assert.IsTrue(i >= 0);


            index_name = n["index_name"];
            Assert.AreEqual("hotel2", index_name);

            var timestamp = n["Timestamp"];
            string sPattern = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z";

            Assert.IsTrue(System.Text.RegularExpressions.Regex.IsMatch(timestamp, sPattern));


        }




    }
}
